# Makefile for StructBASIC
# Compiles the code for the interpreter
# Created on 1/29/2018
# Created by Andrew Davis
#
# Copyright (C) 2018  Andrew Davis
#
# This program is free software: you can redistribute it and/or modify   
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# define the compiler
CXX=g++

# define the compiler flags
CXXFLAGS=-c -std=c++17 -Wall

# define state-specific compiler flags
debug: CXXFLAGS += -g

# define linker flags
LDFLAGS=-lm

# retrieve the source code
MAIN=$(shell ls src/*.cpp)
EXCE=$(shell ls src/except/*.cpp)
UTIL=$(shell ls src/util/*.cpp)
VAR=$(shell ls src/var/*.cpp)
AST=$(shell ls src/ast/*.cpp)
CORE=$(shell ls src/core/*.cpp)
FUNC=$(shell ls src/func/*.cpp)

# list the source code
SOURCES=$(MAIN) $(EXCE) $(UTIL) $(VAR) $(AST) $(CORE) $(FUNC)

# compile the source code
OBJECTS=$(SOURCES:.cpp=.o)

# define the executable name
EXECUTABLE=sbasic

# start of build targets

# compiles the entire project without debug symbols
all: $(SOURCES) $(EXECUTABLE)

# compiles the executable without debug symbols
$(EXECUTABLE): $(OBJECTS)
	$(CXX) $(OBJECTS) -o $@ $(LDFLAGS)
	mkdir bin
	mkdir obj
	mv -f $@ bin/
	mv -f $(OBJECTS) obj/

# compiles the executable with debug symbols
debug: $(OBJECTS)
	$(CXX) $(OBJECTS) -o $(EXECUTABLE) $(LDFLAGS)
	mkdir bin
	mkdir obj
	mv -f $(EXECUTABLE) bin/
	mv -f $(OBJECTS) obj/

# compiles source code into object code
.cpp.o:
	$(CXX) $(CXXFLAGS) $< -o $@

# cleans the workspace
clean:
	rm -rf bin
	rm -rf obj

# installs the compiled interpreter
# REQUIRES ROOT
install:
	cp bin/$(EXECUTABLE) /usr/bin/

# end of Makefile
