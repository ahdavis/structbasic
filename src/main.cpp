/*
 * main.cpp
 * Main code file for StructBASIC
 * Created on 1/29/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include <iostream>
#include <cstdlib>
#include <string>
#include "core/Interpreter.h"
#include "core/Lexer.h"
#include "core/Parser.h"
#include "var/Variant.h"
#include "util/SrcFile.h"
#include "util/data.h"
#include "util/constants.h"

//main function - main entry point for program
int main(int argc, char* argv[]) {
	//handle command-line arguments
	if(argc > 1) { //if a source file was supplied
		SrcFile* file = new SrcFile(argv[1]); //then open the file

		isExec = true; //and set the exec flag

		//loop and evaluate the code in the file
		while(!file->isEOF()) { //loop until EOF
			//read each line of code
			std::string code = file->nextLine();

			//handle a blank line
			if(code == "") {
				continue;
			}

			//evaluate it
			Lexer l = Lexer(code);
			Parser p = Parser(l);
			Interpreter i = Interpreter(p);
			Variant v = i.interpret();
			
			//and print out the result
			std::cout << v;
		}

		//close the file
		delete file;
		file = nullptr;

		//and exit with a success
		return EXIT_SUCCESS;
	}

	//The following code will only execute if no source files
	//are supplied
	
	//reset the exec flag
	isExec = false;
	
	//declare variables
	std::string text; //contains the code being evaluated
	
	//display the startup message
	std::cout << startupMsg << std::endl;

	//loop and evaluate code
	while(true) {
		//display a prompt
		std::cout << "> ";

		//read in a statement
		std::getline(std::cin, text);

		//interpret the statement
		Lexer l(text);
		Parser p(l);
		Interpreter i(p);
		Variant v = i.interpret();

		//print out the result
		std::cout << v << std::endl;
	}
	
	//and return with no errors
	return EXIT_SUCCESS;
}

//end of program
