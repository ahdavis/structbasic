/*
 * fileExistsAtPath.cpp
 * Declares functions that check whether a file exists at a given path
 * Created on 2/4/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "fileExistsAtPath.h"

//first fileExistsAtPath function - checks to see if a file specified
//by a C string exists
bool fileExistsAtPath(const char* path) {
	//get a file object to verify the path with
	FILE* test = fopen(path, "r");

	//and verify that the file exists
	if(test) { //if the file exists
		fclose(test); //then close the file
		return true; //and return a success
	} else { //if the file does not exist
		return false; //then return a failure
	}
}

//second fileExistsAtPath function - checks to see if a file specified
//by an std::string exists
bool fileExistsAtPath(const std::string& path) {
	return fileExistsAtPath(path.c_str()); //call the C string function
}

//third fileExistsAtPath function - checks to see if a file specified
//by a Variant exists
//handles the TypeException thrown if the Variant is not holding a string
bool fileExistsAtPath(const Variant& path) {
	//create a string to hold the path
	std::string pathStr;

	//attempt to check the path
	try {
		pathStr = path.stringValue();
	} catch(const TypeException& te) { //the Variant is not a string
		std::cout << te.what() << std::endl; //print the error
		return false; //and return false
	}

	return fileExistsAtPath(pathStr); //call the std::string function
}


//end of implementation
