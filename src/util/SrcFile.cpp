/*
 * SrcFile.cpp
 * Implements a class that represents a StructBASIC source code file
 * Created on 2/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "SrcFile.h"

//first constructor
SrcFile::SrcFile(const char* fileName)
	: SrcFile(std::string(fileName)) //call the other constructor
{
	//no code needed
}

//second constructor
SrcFile::SrcFile(const std::string& fileName)
	: inFile(), fileName(fileName) //init the fields
{
	//make sure that the file exists
	if(!fileExistsAtPath(fileName)) { //if the file does not exist
		throw SourceException(fileName); //then throw an exception
	}

	//attempt to open the file
	this->inFile.open(fileName);

	//verify that the file was opened successfully
	if(!this->inFile) { //if the file was not opened successfully
		throw SourceException(fileName); //then throw an exception
	}
}

//destructor
SrcFile::~SrcFile() {
	this->close(); //close the file
}

//copy constructor
SrcFile::SrcFile(const SrcFile& sf)
	: inFile(sf.fileName), fileName(sf.fileName) //copy the fields
{
	//no code needed
}

//move constructor
SrcFile::SrcFile(SrcFile&& sf)
	: inFile(sf.fileName), fileName(sf.fileName) //move the fields
{
	//close the temporary
	sf.close();
}

//assignment operator
SrcFile& SrcFile::operator=(const SrcFile& src) {
	this->inFile = std::ifstream(src.fileName); //assign the stream
	this->fileName = src.fileName; //assign the file name
	return *this; //and return the instance
}

//move operator
SrcFile& SrcFile::operator=(SrcFile&& src) {
	this->inFile = std::ifstream(src.fileName); //move the stream
	this->fileName = src.fileName; //move the file name
	src.close(); //close the temporary
	return *this; //and return the instance
}

//nextLine method - returns the next line from the file
std::string SrcFile::nextLine() {
	//declare the return value
	std::string ret;

	//read in the next line from the file
	std::getline(this->inFile, ret);

	//and return the read-in line
	return ret;
}

//isEOF method - returns whether the file has reached EOF
bool SrcFile::isEOF() const {
	return this->inFile.eof(); //return whether the file is EOF
}

//private close method - closes the file
void SrcFile::close() {
	this->inFile.close(); //close the file stream
}

//end of implementation
