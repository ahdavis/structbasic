/*
 * typecheck.cpp
 * Implements functions that check strings for given types
 * Created on 1/30/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "typecheck.h"

//constant strings that contain regexes
const std::string regexInt = "^(\\+|-)?[0-9]+$"; //matches int
const std::string regexFloat = "[\\+-]?([0-9]*[.])?[0-9]+"; //matches float

//checkInt function - checks for an integer
bool checkInt(const std::string& val) {
	std::regex intRegex(regexInt); //get a regex to match an integer
	return std::regex_match(val, intRegex); //and return the match
}

//checkFloat function - checks for a float
bool checkFloat(const std::string& val) {
	std::regex floatRegex(regexFloat); //get a regex to match a float
	return std::regex_match(val, floatRegex); //and return the match
}

//checkChar function - checks for a character
bool checkChar(const std::string& val) {
	return val.size() == 1; //return whether the string is one char
}

//checkBool function - checks for a boolean
bool checkBool(const std::string& val) {
	return (val == "true") || (val == "false");
}

//end of implementation
