/*
 * SrcFile.h
 * Declares a class that represents a StructBASIC source code file
 * Created on 2/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <fstream>
#include <string>
#include "../except/SourceException.h"
#include "fileExistsAtPath.h"

//class declaration
class SrcFile final {
	//public fields and methods
	public:
		//first constructor - constructs from a C string
		explicit SrcFile(const char* fileName);

		//second constructor - constructs from a std::string
		explicit SrcFile(const std::string& fileName);

		//destructor
		~SrcFile();

		//copy constructor
		SrcFile(const SrcFile& sf);

		//move constructor
		SrcFile(SrcFile&& sf);

		//assignment operator
		SrcFile& operator=(const SrcFile& src);

		//move operator
		SrcFile& operator=(SrcFile&& src);

		//other methods
		
		//returns the next line in the file
		std::string nextLine();

		//returns whether the file has reached EOF
		bool isEOF() const;

	//private fields and methods
	private:
		//method
		void close(); //closes the file

		//fields
		std::ifstream inFile; //the file stream to read from
		std::string fileName; //the name of the file
		
};

//end of header
