/*
 * constants.cpp
 * Defines constants for StructBASIC
 * Created on 2/28/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "constants.h"

//constant definitions

//the version of the interpreter
const std::string version = "v. 1.0 alpha";

//the startup message for the REPL
const std::string startupMsg = "StructBASIC " + 
				version + 
				"\nCtrl + C to quit";


//end of definitions
