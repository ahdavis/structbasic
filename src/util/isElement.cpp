/*
 * isElement.cpp
 * Implements a function that determines whether a value is in a vector
 * Created on 2/23/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include <vector>
#include <algorithm>

//isElement function - determines whether a value is in a vector
template<typename T>
bool isElement(T value, const std::vector<T>& vec) {
	//determine whether the vector contains the value
	if(std::find(vec.begin(), vec.end(), value) != vec.end()) {
		return true; //the vector contains the value
	} else {
		return false; //the vector does not contain the value
	}
}

//end of implementation
