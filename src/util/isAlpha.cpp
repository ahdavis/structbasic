/*
 * isAlpha.cpp
 * Implements a function that checks a character to see if it's a letter
 * Created on 2/21/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "isAlpha.h"

//isAlpha function - returns whether a character is a letter
bool isAlpha(char c) {
	//check to see if the character is uppercase or lowercase
	bool isLower = ((c >= 'a') && (c <= 'z')); //character is lowercase
	bool isUpper = ((c >= 'A') && (c <= 'Z')); //character is uppercase

	//and return the logical OR of the two results
	return isLower || isUpper;
}


//end of implementation
