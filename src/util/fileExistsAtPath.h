/*
 * fileExistsAtPath.h
 * Declares functions that check whether a file exists at a given path
 * Created on 2/4/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <cstdlib>
#include <string>
#include <iostream>
#include "../var/Variant.h"
#include "../except/TypeException.h"

//function declarations

//checks to see if a file specified by a C string exists
bool fileExistsAtPath(const char* path);

//checks to see if a file specified by an std::string exists
bool fileExistsAtPath(const std::string& path);

//checks to see if a file specified by a Variant exists
//handles the TypeException thrown if the Variant is not holding a string
bool fileExistsAtPath(const Variant& path);

//end of header
