/*
 * functions.h
 * Declares built-in functions for StructBASIC
 * Created on 2/25/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <iostream>
#include <memory>
#include <string>
#include <limits>
#include "../ast/NodeType.h"
#include "../ast/ASTNode.h"
#include "../var/Variable.h"
#include "../var/Variant.h"
#include "../var/VarRegistry.h"
#include "../util/data.h"

//function prototypes

//prints out the value of an AST without a newline
void print(const std::shared_ptr<ASTNode>& ast);

//prints out the value of an AST with a newline
void println(const std::shared_ptr<ASTNode>& ast);

//reads in a value from stdin and returns it
Variant input(const std::shared_ptr<ASTNode>& prompt = nullptr);

//evaluates code stored in an AST
Variant eval(const std::shared_ptr<ASTNode>& code);

//end of header
