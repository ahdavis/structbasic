/*
 * functions.cpp
 * Implements built-in functions for StructBASIC
 * Created on 2/25/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "functions.h"

//includes
#include "../core/Interpreter.h"
#include "../core/Lexer.h"
#include "../core/Parser.h"

//print function - prints out the value of an AST without a newline
void print(const std::shared_ptr<ASTNode>& ast) {
	//declare the output value
	Variant out;
	
	//handle variable ASTs
	if(ast->getNodeType() == NodeType::VAR) { //AST is a variable ref
		//get the variable name
		std::string varName = ast->eval().stringValue();

		//and assign the output value using the variable
		out = VarRegistry::getInstance().lookup(varName).getValue();
	} else { //AST is not a variable ref
		//assign the output value
		out = ast->eval();
	}

	//stream out the evaluated value
	std::cout << out;
}

//println function - prints out the value of an AST with a newline
void println(const std::shared_ptr<ASTNode>& ast) {
	//declare the output value
	Variant out;
	
	//handle variable ASTs
	if(ast->getNodeType() == NodeType::VAR) { //AST is a variable ref
		//get the variable name
		std::string varName = ast->eval().stringValue();

		//and assign the output value using the variable
		out = VarRegistry::getInstance().lookup(varName).getValue();
	} else { //AST is not a variable ref
		//assign the output value
		out = ast->eval();
	}

	//stream out the evaluated value
	std::cout << out << std::endl;
}

//input function - reads in input from stdin
Variant input(const std::shared_ptr<ASTNode>& prompt) {
	//declare a value to read into
	Variant in;

	//print out the prompt
	if(prompt) { //if the prompt exists
		print(prompt); //then print it
	}

	//read in input from stdin
	getline(std::cin, in);

	//and return the read in value
	return in;
}

//eval function - evaluates code stored in a Variant
Variant eval(const std::shared_ptr<ASTNode>& code) {
	//check for the execute flag
	bool flagWasSet = false; //was the execute flag set?
	if(!isExec) { //if the execute flag is reset
		isExec = true; //then set it
		flagWasSet = true; //and set the signifier flag
	}

	//evaluate the AST argument
	Variant evalCode;
	if(code->getNodeType() == NodeType::VAR) { //check for a variable
		Variable value = VarRegistry::getInstance()
						.lookup(code->eval()
							.stringValue());
		evalCode = value.getValue();
	} else { //not a variable
		evalCode = code->eval();
	}

	//evaluate the code
	Lexer l(evalCode.stringValue());
	Parser p(l);
	Interpreter i(p);
	Variant ret = i.interpret();

	//restore the execute flag
	if(flagWasSet) {
		isExec = false;
	}

	//and return the evaluated result
	return ret;
}

//end of implementation
