/*
 * ForNode.cpp
 * Implements a class that represents a for loop AST node
 * Created on 3/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "ForNode.h"

//constructor
ForNode::ForNode(const std::shared_ptr<ASTNode>& newInit,
		const std::shared_ptr<ASTNode>& newCond,
		const std::shared_ptr<ASTNode>& newInc,
		const std::shared_ptr<ASTNode>& newBody)
	: ASTNode(NodeType::LOOP, nullptr, nullptr),
		init(newInit), cond(newCond), inc(newInc), body(newBody)
{
	//no code needed
}

//destructor
ForNode::~ForNode() {
	//no code needed
}

//copy constructor
ForNode::ForNode(const ForNode& fn)
	: ASTNode(fn), init(fn.init), 
		cond(fn.cond), inc(fn.inc), body(fn.body)
{
	//no code needed
}

//move constructor
ForNode::ForNode(ForNode&& fn)
	: ASTNode(fn), init(fn.init),
		cond(fn.cond), inc(fn.inc), body(fn.body)
{
	//no code needed
}

//assignment operator
ForNode& ForNode::operator=(const ForNode& src) {
	ASTNode::operator=(src); //call the superclass assignment operator
	this->init = src.init; //assign the initializer
	this->cond = src.cond; //assign the conditional
	this->inc = src.inc; //assign the incrementer
	this->body = src.body; //assign the body
	return *this; //and return the instance
}

//move operator
ForNode& ForNode::operator=(ForNode&& src) {
	ASTNode::operator=(src); //call the superclass move operator
	this->init = src.init; //move the initializer
	this->cond = src.cond; //move the conditional
	this->inc = src.inc; //move the incrementer
	this->body = src.body; //move the body
	return *this; //and return the instance
}

//overridden eval method - evaluates the node
Variant ForNode::eval() const {
	//declare the return value
	Variant ret;
	
	//initialize the loop
	this->init->eval();

	//use a while loop to evaluate the loop
	while(this->cond->eval().boolValue()) {
		ret = this->body->eval(); //evaluate and store the body
		this->inc->eval(); //evaluate the incrementer
	}

	//return the return value
	if(!isExec) { //if the interpreter is not in execute mode
		return ret; //then return the loop's result
	} else { //if the interpreter is in execute mode
		return '\0'; //then return a null character
	}
}

//end of implementation
