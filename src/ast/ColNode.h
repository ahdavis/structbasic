/*
 * ColNode.h
 * Declares a class that represents a colon AST node
 * Created on 3/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <memory>
#include "ASTNode.h"
#include "NodeType.h"
#include "../var/Variant.h"

//class declaration
class ColNode final : public ASTNode
{
	//public fields and methods
	public:
		//constructor
		ColNode(const std::shared_ptr<ASTNode>& newLeft,
			const std::shared_ptr<ASTNode>& newRight);

		//destructor
		~ColNode();

		//copy constructor
		ColNode(const ColNode& cn);

		//move constructor
		ColNode(ColNode&& cn);

		//assignment operator
		ColNode& operator=(const ColNode& src);

		//move operator
		ColNode& operator=(ColNode&& src);

		//evaluates the node
		Variant eval() const override;

	//no private fields or methods
};

//end of header
