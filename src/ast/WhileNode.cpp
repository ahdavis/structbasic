/*
 * WhileNode.cpp
 * Implements a class that represents a while loop AST node
 * Created on 3/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "WhileNode.h"

//constructor
WhileNode::WhileNode(const std::shared_ptr<ASTNode>& newCond,
			const std::shared_ptr<ASTNode>& newBody)
	: ASTNode(NodeType::LOOP, nullptr, nullptr), cond(newCond),
		body(newBody)
{
	//no code needed
}

//destructor
WhileNode::~WhileNode() {
	//no code needed
}

//copy constructor
WhileNode::WhileNode(const WhileNode& wn)
	: ASTNode(wn), cond(wn.cond), body(wn.body) //copy the fields
{
	//no code needed
}

//move constructor
WhileNode::WhileNode(WhileNode&& wn)
	: ASTNode(wn), cond(wn.cond), body(wn.body) //move the fields
{
	//no code needed
}

//assignment operator
WhileNode& WhileNode::operator=(const WhileNode& src) {
	ASTNode::operator=(src); //call the superclass assignment operator
	this->cond = src.cond; //assign the conditional
	this->body = src.body; //assign the body
	return *this; //and return the instance
}

//move operator
WhileNode& WhileNode::operator=(WhileNode&& src) {
	ASTNode::operator=(src); //call the superclass move operator
	this->cond = src.cond; //move the conditional
	this->body = src.body; //assign the body
	return *this; //and return the instance
}

//overridden eval method - evaluates the node
Variant WhileNode::eval() const {
	//declare the return value
	Variant ret;

	//evaluate the loop
	while(this->cond->eval().boolValue()) {
		ret = this->body->eval(); //evaluate and store the body
	}

	//return the return value
	if(!isExec) { //if the interpreter is not in execute mode
		return ret; //then return the return value
	} else { //if the interpreter is in execute mode
		return '\0'; //then return a null
	}
}

//end of implementation
