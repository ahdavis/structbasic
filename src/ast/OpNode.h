/*
 * OpNode.h
 * Declares a class that represents an operational AST node in StructBASIC
 * Created on 2/4/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "OpType.h"
#include "NodeType.h"
#include "ASTNode.h"
#include <memory>
#include "../except/UnknownOpException.h"
#include "../except/ConstException.h"
#include "../except/DivByZeroException.h"
#include "../var/VarRegistry.h"
#include "../var/Variable.h"
#include "../var/Variant.h"

//class declaration
class OpNode final : public ASTNode
{
	//public fields and methods
	public:
		//constructor
		OpNode(OpType newOpType, 
				const std::shared_ptr<ASTNode>& newLeft,
				const std::shared_ptr<ASTNode>& newRight);

		//destructor
		~OpNode();

		//copy constructor
		OpNode(const OpNode& op);

		//move constructor
		OpNode(OpNode&& op);

		//assignment operator
		OpNode& operator=(const OpNode& src);

		//move operator
		OpNode& operator=(OpNode&& src);

		//getter method
		
		//evaluates the operation
		Variant eval() const override;

	//private fields and methods
	private:
		//field
		OpType opType; //the operation type for the node
};

//end of header
