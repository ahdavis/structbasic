/*
 * ColNode.cpp
 * Implements a class that represents a colon AST node
 * Created on 3/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "ColNode.h"

//constructor
ColNode::ColNode(const std::shared_ptr<ASTNode>& newLeft,
			const std::shared_ptr<ASTNode>& newRight)
	: ASTNode(NodeType::COL, newLeft, newRight) 
{
	//no code needed
}

//destructor
ColNode::~ColNode() {
	//no code needed
}

//copy constructor
ColNode::ColNode(const ColNode& cn)
	: ASTNode(cn) //call the superclass copy constructor
{
	//no code needed
}

//move constructor
ColNode::ColNode(ColNode&& cn)
	: ASTNode(cn) //call the superclass move constructor
{
	//no code needed
}

//assignment operator
ColNode& ColNode::operator=(const ColNode& src) {
	ASTNode::operator=(src); //call the superclass assignment operator
	return *this; //and return the instance
}

//move operator
ColNode& ColNode::operator=(ColNode&& src) {
	ASTNode::operator=(src); //call the superclass move operator
	return *this; //and return the instance
}

//overridden eval method - evaluates the node
Variant ColNode::eval() const {
	//evaluate the left branch
	this->left->eval();

	//and return the evaluation of the right branch
	return this->right->eval();
}

//end of implementation
