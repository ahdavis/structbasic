/*
 * VarNode.cpp
 * Implements a class that represents a variable AST node in StructBASIC
 * Created on 2/21/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "VarNode.h"

//constructor
VarNode::VarNode(const std::string& newRef)
	: ASTNode(NodeType::VAR, nullptr, nullptr), refName(newRef)
{
	//no code needed
}

//destructor
VarNode::~VarNode() {
	//no code needed
}

//copy constructor
VarNode::VarNode(const VarNode& var)
	: ASTNode(var), refName(var.refName) //copy the fields
{
	//no code needed
}

//move constructor
VarNode::VarNode(VarNode&& var)
	: ASTNode(var), refName(var.refName) //move the fields
{
	//no code needed
}

//assignment operator
VarNode& VarNode::operator=(const VarNode& src) {
	ASTNode::operator=(src); //call the superclass assignment operator
	this->refName = src.refName; //assign the reference name
	return *this; //and return the instance
}

//move operator
VarNode& VarNode::operator=(VarNode&& src) {
	ASTNode::operator=(src); //call the superclass move operator
	this->refName = src.refName; //move the reference name
	return *this; //and return the instance
}

//overridden eval method - evaluates the node
Variant VarNode::eval() const {
	//return the reference name as a Variant
	return Variant(this->refName); 
}

//end of implementation
