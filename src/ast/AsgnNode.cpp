/*
 * AsgnNode.cpp
 * Implements a class that represents an assignment AST node in StructBASIC
 * Created on 2/22/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "AsgnNode.h"

//constructor
AsgnNode::AsgnNode(const std::shared_ptr<ASTNode>& newLeft,
			const std::shared_ptr<ASTNode>& newRight)
	: ASTNode(NodeType::ASGN, newLeft, newRight) //init the fields
{
	//no code needed
}

//destructor
AsgnNode::~AsgnNode() {
	//no code needed
}

//copy constructor
AsgnNode::AsgnNode(const AsgnNode& node)
	: ASTNode(node) //call the superclass copy constructor
{
	//no code needed
}

//move constructor
AsgnNode::AsgnNode(AsgnNode&& node)
	: ASTNode(node) //call the superclass move constructor
{
	//no code needed
}

//assignment operator
AsgnNode& AsgnNode::operator=(const AsgnNode& src) {
	ASTNode::operator=(src); //call the superclass assignment operator
	return *this; //and return the instance
}

//move operator
AsgnNode& AsgnNode::operator=(AsgnNode&& src) {
	ASTNode::operator=(src); //call the superclass move operator
	return *this; //and return the instance
}

//overridden eval method - evaluates the node
Variant AsgnNode::eval() const {
	//make sure that the left branch is a variable reference
	if(this->left->getNodeType() != NodeType::VAR) { //not a variable
		//throw an exception
		throw AssignException();
	}

	//get the name of the variable to assign
	std::string varName = this->left->eval().stringValue();

	//declare the variable to store
	Variable storeVar;

	//handle variable-to-variable assignment
	if(this->right->getNodeType() == NodeType::VAR) {
		//get the name of the right branch
		std::string rName = this->right->eval().stringValue();
		
		//get the value of the right branch
		Variant value = VarRegistry::getInstance()
					.lookup(rName).getValue();

		//and create the variable to store
		storeVar = Variable(varName, value);
	} else { //not a variable
		//create the variable to store
		storeVar = Variable(varName, this->right->eval());
	}

	//store the created variable
	VarRegistry::getInstance().store(storeVar);

	//and return the variable's value if the exec flag is reset
	if(isExec) {
		return '\0';
	} else {
		return storeVar.getValue();
	}
}

//end of implementation
