/*
 * ASTNode.cpp
 * Implements a class that represents a general AST node in StructBASIC
 * Created on 2/4/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "ASTNode.h"

//constructor
ASTNode::ASTNode(NodeType newType, 
		const std::shared_ptr<ASTNode>& newLeft,
		const std::shared_ptr<ASTNode>& newRight)
	: type(newType), left(newLeft), right(newRight) //init the fields
{
	//no code needed
}

//destructor
ASTNode::~ASTNode() {
	//no code needed
}

//copy constructor
ASTNode::ASTNode(const ASTNode& ast)
	: type(ast.type), left(ast.left), right(ast.right) //copy fields
{
	//no code needed
}

//move constructor
ASTNode::ASTNode(ASTNode&& ast)
	: type(ast.type), left(ast.left), right(ast.right) //move fields
{
	//no code needed
}

//assignment operator
ASTNode& ASTNode::operator=(const ASTNode& src) {
	//assign the fields
	this->type = src.type; //assign the type field
	this->left = src.left; //assign the left field
	this->right = src.right; //assign the right field

	//and return the instance
	return *this;
}

//move operator
ASTNode& ASTNode::operator=(ASTNode&& src) {
	//move the fields
	this->type = src.type; //move the type field
	this->left = src.left; //move the left field
	this->right = src.right; //move the right field

	//and return the instance
	return *this;
}

//getNodeType method - returns the type of the AST node
NodeType ASTNode::getNodeType() const {
	return this->type; //return the type field
}


//end of implementation
