/*
 * IfNode.h
 * Declares a class that represents an if statement AST node
 * Created on 3/4/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <memory>
#include "ASTNode.h"
#include "NodeType.h"
#include "../var/Variant.h"
#include "../util/data.h"

//class declaration
class IfNode final : public ASTNode
{
	//public fields and methods
	public:
		//constructor
		IfNode(const std::shared_ptr<ASTNode>& newCond,
			const std::shared_ptr<ASTNode>& newThen,
			const std::shared_ptr<ASTNode>& newElse);

		//destructor
		~IfNode();

		//copy constructor
		IfNode(const IfNode& in);

		//move constructor
		IfNode(IfNode&& in);

		//assignment operator
		IfNode& operator=(const IfNode& src);

		//move operator
		IfNode& operator=(IfNode&& src);

		//evaluates the node
		Variant eval() const;

	//private fields and methods
	private:
		//fields
		std::shared_ptr<ASTNode> cond; //the conditional for the if
		std::shared_ptr<ASTNode> then; //the then branch for the if
		std::shared_ptr<ASTNode> els; //the else branch for the if
};

//end of header
