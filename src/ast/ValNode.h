/*
 * ValNode.h
 * Declares a class that represents a constant-value AST node in StructBASIC
 * Created on 2/4/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "ASTNode.h"
#include "NodeType.h"
#include "../var/Variant.h"

//class declaration
class ValNode final : public ASTNode
{
	//public fields and methods
	public:
		//constructor
		explicit ValNode(const Variant& newValue);

		//destructor
		~ValNode();

		//copy constructor
		ValNode(const ValNode& val);

		//move constructor
		ValNode(ValNode&& val);

		//assignment operator
		ValNode& operator=(const ValNode& src);

		//move operator
		ValNode& operator=(ValNode&& src);

		//getter method
		
		//returns the value stored in the node
		Variant eval() const override;

	//private fields and methods
	private:
		//field
		Variant value; //the value stored in the node
};

//end of header
