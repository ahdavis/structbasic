/*
 * ForNode.h
 * Declares a class that represents a for loop AST node
 * Created on 3/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <memory>
#include <string>
#include "ASTNode.h"
#include "NodeType.h"
#include "../util/data.h"


//class declaration
class ForNode final : public ASTNode
{
	//public fields and methods
	public:
		//constructor
		ForNode(const std::shared_ptr<ASTNode>& newInit,
			const std::shared_ptr<ASTNode>& newCond,
			const std::shared_ptr<ASTNode>& newInc,
			const std::shared_ptr<ASTNode>& newBody);

		//destructor
		~ForNode();

		//copy constructor
		ForNode(const ForNode& fn);

		//move constructor
		ForNode(ForNode&& fn);

		//assignment operator
		ForNode& operator=(const ForNode& src);

		//move operator
		ForNode& operator=(ForNode&& src);

		//evaluates the node
		Variant eval() const;
	
	//private fields and methods
	private:
		//fields
		std::shared_ptr<ASTNode> init; //the loop's initializer
		std::shared_ptr<ASTNode> cond; //the loop's conditional
		std::shared_ptr<ASTNode> inc; //the loop's incrementer
		std::shared_ptr<ASTNode> body; //the loop's body

};

//end of header
