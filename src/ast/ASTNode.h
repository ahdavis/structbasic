/*
 * ASTNode.h
 * Declares a class that represents a general AST node in StructBASIC
 * Created on 2/4/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <memory>
#include "NodeType.h"
#include "../var/Variant.h"

//class declaration
class ASTNode {
	//public fields and methods
	public:
		//constructor
		ASTNode(NodeType newType, 
				const std::shared_ptr<ASTNode>& newLeft, 
				const std::shared_ptr<ASTNode>& newRight);

		//destructor
		virtual ~ASTNode();

		//copy constructor
		ASTNode(const ASTNode& ast);

		//move constructor
		ASTNode(ASTNode&& ast);

		//assignment operator
		ASTNode& operator=(const ASTNode& src);

		//move operator
		ASTNode& operator=(ASTNode&& src);

		//getter methods
		
		//returns the type of the AST node
		NodeType getNodeType() const;

		//evaluates the node
		virtual Variant eval() const = 0;

	//protected fields and methods
	protected:
		//fields
		NodeType type; //the type of the AST
		std::shared_ptr<ASTNode> left; //the left branch of the AST
		std::shared_ptr<ASTNode> right; //the right branch
};

//end of header
