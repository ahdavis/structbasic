/*
 * CmpNode.cpp
 * Implements a class that represents a comparison AST node in StructBASIC
 * Created on 2/23/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "CmpNode.h"

//constructor
CmpNode::CmpNode(CmpType newType, const std::shared_ptr<ASTNode>& newLeft,
			const std::shared_ptr<ASTNode>& newRight)
	: ASTNode(NodeType::CMP, newLeft, newRight), cmpType(newType)
{
	//no code needed
}

//destructor
CmpNode::~CmpNode() {
	//no code needed
}

//copy constructor
CmpNode::CmpNode(const CmpNode& cn)
	: ASTNode(cn), cmpType(cn.cmpType) //copy the fields
{
	//no code needed
}

//move constructor
CmpNode::CmpNode(CmpNode&& cn)
	: ASTNode(cn), cmpType(cn.cmpType) //move the fields
{
	//no code needed
}

//assignment operator
CmpNode& CmpNode::operator=(const CmpNode& src) {
	ASTNode::operator=(src); //call the superclass assignment operator
	this->cmpType = src.cmpType; //assign the comparison type
	return *this; //and return the instance
}

//move operator
CmpNode& CmpNode::operator=(CmpNode&& src) {
	ASTNode::operator=(src); //call the superclass move operator
	this->cmpType = src.cmpType; //move the comparison type
	return *this; //and return the instance
}

//overridden eval method - evaluates the node
Variant CmpNode::eval() const {
	//declare the values of the left and right sides
	Variant leftSide;
	Variant rightSide;

	//determine whether the left side is a variable
	if(this->left->getNodeType() == NodeType::VAR) { //it's a variable
		leftSide = VarRegistry::getInstance()
				.lookup(this->left->eval()
						.stringValue()).getValue();
	} else { //not a variable
		leftSide = this->left->eval();
	}

	//determine whether the right side is a variable
	if(this->right->getNodeType() == NodeType::VAR) { //it's a variable
		rightSide = VarRegistry::getInstance()
				.lookup(this->right->eval()
						.stringValue()).getValue();
	} else { //not a variable
		rightSide = this->right->eval();
	}

	//declare a return variable
	Variant ret = false;

	//evaluate the node
	switch(this->cmpType) { //switch on the comparison type
		case CmpType::EQU: //equality
			{
				//evaluate the comparison
				ret = leftSide == rightSide;

				break;
			}
		case CmpType::GT: //greater than
			{
				//evaluate the comparison
				ret = leftSide > rightSide;

				break;
			}
		case CmpType::GTE: //greater than or equal to
			{
				//evaluate the comparison
				ret = leftSide >= rightSide;

				break;
			}
		case CmpType::LT: //less than
			{
				//evaluate the comparison
				ret = leftSide < rightSide;

				break;
			}
		case CmpType::LTE: //less than or equal to
			{
				//evaluate the comparison
				ret = leftSide <= rightSide;

				break;
			}
		case CmpType::NEQ: //not equal
			{
				//evaluate the comparison
				ret = leftSide != rightSide;

				break;
			}
		default: //unknown comparison
			{
				ret = false; //TODO: Create an exception

				break;
			}
	}

	//return the return value
	return ret;

}

//end of implementation
