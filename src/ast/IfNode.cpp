/*
 * IfNode.cpp
 * Implements a class that represents an if statement AST node
 * Created on 3/4/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "IfNode.h"

//constructor
IfNode::IfNode(const std::shared_ptr<ASTNode>& newCond,
		const std::shared_ptr<ASTNode>& newThen,
		const std::shared_ptr<ASTNode>& newElse)
	: ASTNode(NodeType::CTL, nullptr, nullptr), cond(newCond),
		then(newThen), els(newElse)
{
	//no code needed
}

//destructor
IfNode::~IfNode() {
	//no code needed
}

//copy constructor
IfNode::IfNode(const IfNode& in)
	: ASTNode(in), cond(in.cond), then(in.then), els(in.els)
{
	//no code needed
}

//move constructor
IfNode::IfNode(IfNode&& in)
	: ASTNode(in), cond(in.cond), then(in.then), els(in.els)
{
	//no code needed
}

//assignment operator
IfNode& IfNode::operator=(const IfNode& src) {
	ASTNode::operator=(src); //call the superclass assignment operator
	this->cond = src.cond; //assign the condition
	this->then = src.then; //assign the then branch
	this->els = src.els; //assign the else branch
	return *this; //and return the instance
}

//move operator
IfNode& IfNode::operator=(IfNode&& src) {
	ASTNode::operator=(src); //call the superclass move operator
	this->cond = src.cond; //move the condition
	this->then = src.then; //move the then branch
	this->els = src.els; //move the else branch
	return *this; //and return the instance
}

//overridden eval method - evaluates the node
Variant IfNode::eval() const {
	//declare the return value
	Variant ret = '\0';

	//handle whether the statement has an else branch
	if(this->els != nullptr) { //if the statement has an else branch
		//then evaluate the conditional
		if(this->cond->eval().boolValue()) { //statement is true
			ret = this->then->eval(); //then evaluate the branch
		} else { //statement is false
			ret = this->els->eval(); //then evaluate the branch
		}
	} else { //if the statement does not have an else brancg
		//then evaluate the conditional
		if(this->cond->eval().boolValue()) { //statement is true
			ret = this->then->eval(); //then evaluate the branch
		}
	}

	//determine whether to return a null or not
	if(!isExec) { //if the interpreter is not in execute mode
		return ret; //then return the result of the evaluation
	} else { //if the interpreter is in execute mode
		return '\0'; //then return a null character
	}
}

//end of implementation
