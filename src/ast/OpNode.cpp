/*
 * OpNode.cpp
 * Implements a class that represents an operational AST node in StructBASIC
 * Created on 2/4/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "OpNode.h"

//constructor
OpNode::OpNode(OpType newOpType, const std::shared_ptr<ASTNode>& newLeft,
			const std::shared_ptr<ASTNode>& newRight)
	: ASTNode(NodeType::OP, newLeft, newRight), opType(newOpType)
{
	//no code needed
}

//destructor
OpNode::~OpNode() {
	//no code needed
}

//copy constructor
OpNode::OpNode(const OpNode& op)
	: ASTNode(op), opType(op.opType) //copy the fields
{
	//no code needed
}

//move constructor
OpNode::OpNode(OpNode&& op)
	: ASTNode(op), opType(op.opType) //move the fields
{
	//no code needed
}

//assignment operator
OpNode& OpNode::operator=(const OpNode& src) {
	ASTNode::operator=(src); //call the superclass assignment operator
	this->opType = src.opType; //assign the operation type
	return *this; //and return the instance
}

//move operator
OpNode& OpNode::operator=(OpNode&& src) {
	ASTNode::operator=(src); //call the superclass move operator
	this->opType = src.opType; //move the operation type
	return *this; //and return the instance
}

//overridden eval method - evaluates the node
Variant OpNode::eval() const {
	//determine whether the node is an increment or decrement node
	if(this->opType == OpType::INC) { //increment node
		//make sure that the left branch stores a variable
		if(this->left->getNodeType() != NodeType::VAR) {
			//throw an exception
			throw ConstException();
		}

		//get the variable to increment
		Variable incVar = VarRegistry::getInstance()
					.lookup(this->left->eval()
							.stringValue());

		//increment the variable
		++incVar;

		//store it back in the registry
		VarRegistry::getInstance().store(incVar);
		
		//and return its value
		return incVar.getValue();

	} else if(this->opType == OpType::DEC) { //decrement node
		//make sure that the left branch stores a variable
		if(this->left->getNodeType() != NodeType::VAR) {
			//throw an exception
			throw ConstException();
		}

		//get the variable to decrement
		Variable decVar = VarRegistry::getInstance()
					.lookup(this->left->eval()
							.stringValue());

		//decrement the variable
		--decVar;

		//store it back in the registry
		VarRegistry::getInstance().store(decVar);
		
		//and return its value
		return decVar.getValue();
	}

	//get the left and right sides
	Variant leftSide;
	Variant rightSide;

	//determine whether the left side is a variable reference
	if(this->left->getNodeType() == NodeType::VAR) { //it's a variable
		//get its value
		leftSide = VarRegistry::getInstance()
				.lookup(this->left->eval().stringValue())
					.getValue();
	} else { //not a variable
		//get the left side's value
		leftSide = this->left->eval();
	}

	//determine whether the right side is a variable reference
	if(this->right != nullptr) { //if the right side exists
		if(this->right->getNodeType() == NodeType::VAR) { //var
			//get its value
			rightSide = VarRegistry::getInstance()
					.lookup(this->right->eval()
						.stringValue())
					       		.getValue();
		} else { //not a variable
			//get the right side's value
			rightSide = this->right->eval();
		}
	}
	
	//switch on the op type
	switch(this->opType) {
		case OpType::ADD: //addition
			{
				//evaluate the addition
				leftSide += rightSide;

				break;
			}
		case OpType::DIV: //division
			{
				//check for division by zero
				if((rightSide == 0) || (rightSide == 0.0)) {
					throw DivByZeroException();
				}

				//evaluate the division
				leftSide /= rightSide;

				break;
			}
		case OpType::MOD: //modulus
			{
				//evaluate the modulus
				leftSide %= rightSide;

				break;
			}
		case OpType::MPY: //multiplication
			{
				//evaluate the multiplication
				leftSide *= rightSide;
				break;
			}
		case OpType::NEG: //negation
			{
				//evaluate the negation
				leftSide = -leftSide;

				break;
			}
		case OpType::SUB: //subtraction
			{
				//evaluate the subtraction
				leftSide -= rightSide;

				break;
			}
		default: //unknown operator
			{
				//throw an exception
				throw UnknownOpException();
			}
	}

	//return the calculated return value
	return leftSide;
}

//end of implementation
