/*
 * OpType.h
 * Enumerates types of operations in StructBASIC
 * Created on 2/4/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//enum definition
enum class OpType {
	ADD, //addition/concatenation
	SUB, //subtraction
	MPY, //multiplication
	DIV, //division
	MOD, //modulus
	NEG, //negation
	INC, //incrementation
	DEC, //decrementation
};

//end of enum
