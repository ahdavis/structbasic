/*
 * ValNode.cpp
 * Implements a class that represents a constant-value 
 * AST node in StructBASIC
 * Created on 2/4/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "ValNode.h"

//constructor
ValNode::ValNode(const Variant& newValue)
	: ASTNode(NodeType::VAL, nullptr, nullptr), value(newValue)
{
	//no code needed
}

//destructor
ValNode::~ValNode() {
	//no code needed
}

//copy constructor
ValNode::ValNode(const ValNode& val)
	: ASTNode(val), value(val.value) //copy the fields
{
	//no code needed
}

//move constructor
ValNode::ValNode(ValNode&& val)
	: ASTNode(val), value(val.value) //move the fields
{
	//no code needed
}

//assignment operator
ValNode& ValNode::operator=(const ValNode& src) {
	ASTNode::operator=(src); //call the superclass assignment operator
	this->value = src.value; //assign the value field
	return *this; //and return the instance
}

//move operator
ValNode& ValNode::operator=(ValNode&& src) {
	ASTNode::operator=(src); //call the superclass move operator
	this->value = src.value; //move the value field
	return *this; //and return the instance
}

//overridden eval method - evaluates the node
Variant ValNode::eval() const {
	return this->value; //return the value field
}

//end of implementation
