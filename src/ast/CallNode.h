/*
 * CallNode.h
 * Declares a class that represents a call to a built-in function
 * Created on 2/25/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../util/defs.h"
#include "ASTNode.h"
#include "NodeType.h"
#include "../func/functions.h"
#include "../func/FuncID.h"
#include "../var/Variant.h"
#include "../except/FuncException.h"

//class declaration
class CallNode final : public ASTNode
{
	//public fields and methods
	public:
		//constructor
		CallNode(FuncID newID, ArgList newArgs = ArgList());

		//destructor
		~CallNode();

		//copy constructor
		CallNode(const CallNode& cn);

		//move constructor
		CallNode(CallNode&& cn);

		//assignment operator
		CallNode& operator=(const CallNode& src);

		//move operator
		CallNode& operator=(CallNode&& src);

		//evaluates the node
		Variant eval() const override;

	//private fields and methods
	private:
		//fields
		FuncID funcID; //the ID of the function to call
		ArgList args; //the arguments to the function
};

//end of header
