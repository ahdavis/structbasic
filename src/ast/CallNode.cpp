/*
 * CallNode.cpp
 * Implements a class that represents a call to a built-in function
 * Created on 2/25/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "CallNode.h"

//constructor
CallNode::CallNode(FuncID newID, ArgList newArgs)
	: ASTNode(NodeType::CALL, nullptr, nullptr), funcID(newID), 
		args(newArgs)
{
	//no code needed
}

//destructor
CallNode::~CallNode() {
	//no code needed
}

//copy constructor
CallNode::CallNode(const CallNode& cn)
	: ASTNode(cn), funcID(cn.funcID), args(cn.args) //copy the fields
{
	//no code needed
}

//move constructor
CallNode::CallNode(CallNode&& cn)
	: ASTNode(cn), funcID(cn.funcID), args(cn.args) //move the fields
{
	//no code needed
}

//assignment operator
CallNode& CallNode::operator=(const CallNode& src) {
	ASTNode::operator=(src); //call the superclass assignment operator
	this->funcID = src.funcID; //assign the function ID
	this->args = src.args; //assign the arguments
	return *this; //and return the instance
}

//move operator
CallNode& CallNode::operator=(CallNode&& src) {
	ASTNode::operator=(src); //call the superclass move operator
	this->funcID = src.funcID; //move the function ID
	this->args = src.args; //move the arguments
	return *this; //and return the instance
}

//overridden eval method - evaluates the node
Variant CallNode::eval() const {
	//declare a return value
	Variant ret = "nil";

	//determine which function to call
	switch(this->funcID) {
		case FuncID::INPUT: //input function
			{
				//determine whether to use the arguments
				if(this->args.size() == 0) {
					ret = input(); //read in the input
				} else { //arguments defined
					//read in the input
					//after displaying a prompt
					ret = input(args[0]);
				}

				break;
			}
		case FuncID::PRINT: //print function
			{
				//determine whether there are actually
				//arguments supplied
				if(this->args.size() == 0) {
					//no arguments, so throw
					//an exception
					throw FuncException();
				}

				//evaluate the function
				print(this->args[0]);

				//and update the return value
				ret = '\0';

				break;
			}
		case FuncID::PRINTLN: //println function
			{
				//determine whether there are actually
				//arguments supplied
				if(this->args.size() == 0) {
					//no arguments, so throw
					//an exception
					throw FuncException();
				}

				//evaluate the function
				println(this->args[0]);

				//and update the return value
				ret = '\0';

				break;
			}
		case FuncID::EVAL: //eval function
			{
				//determine whether arguments
				//are supplied
				if(this->args.size() < 1) {
					//no arguments supplied,
					//so throw an exception
					throw FuncException();
				}

				//evaluate the function
				ret = ::eval(this->args[0]);

				break;
			}
		default: //unknown function
			{
				//throw an exception
				throw FuncException();

				break;
			}
	}

	//and return the return value
	return ret;
}

//end of implementation
