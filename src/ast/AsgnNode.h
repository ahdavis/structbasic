/*
 * AsgnNode.h
 * Declares a class that represents an assignment AST node in StructBASIC
 * Created on 2/22/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <memory>
#include <string>
#include "ASTNode.h"
#include "NodeType.h"
#include "VarNode.h"
#include "../var/Variant.h"
#include "../var/Variable.h"
#include "../var/VarRegistry.h"
#include "../except/AssignException.h"
#include "../util/data.h"

//class declaration
class AsgnNode final : public ASTNode
{
	//public fields and methods
	public:
		//constructor
		AsgnNode(const std::shared_ptr<ASTNode>& newLeft,
				const std::shared_ptr<ASTNode>& newRight);

		//destructor
		~AsgnNode();

		//copy constructor
		AsgnNode(const AsgnNode& node);

		//move constructor
		AsgnNode(AsgnNode&& node);

		//assignment operator
		AsgnNode& operator=(const AsgnNode& src);

		//move operator
		AsgnNode& operator=(AsgnNode&& src);

		//evaluates the node
		Variant eval() const override;

	//no private fields or methods
};


//end of header
