/*
 * CmpNode.h
 * Declares a class that represents a comparison AST node in StructBASIC
 * Created on 2/23/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <memory>
#include "ASTNode.h"
#include "CmpType.h"
#include "NodeType.h"
#include "../var/Variable.h"
#include "../var/Variant.h"
#include "../var/VarRegistry.h"

//class declaration
class CmpNode final : public ASTNode
{
	//public fields and methods
	public:
		//constructor
		CmpNode(CmpType newType, 
				const std::shared_ptr<ASTNode>& newLeft,
				const std::shared_ptr<ASTNode>& newRight);

		//destructor
		~CmpNode();

		//copy constructor
		CmpNode(const CmpNode& cn);

		//move constructor
		CmpNode(CmpNode&& cn);

		//assignment operator
		CmpNode& operator=(const CmpNode& src);

		//move operator
		CmpNode& operator=(CmpNode&& src);

		//evaluates the node
		Variant eval() const override;

	//private fields and methods
	private:
		//field
		CmpType cmpType; //the comparison type
};

//end of header
