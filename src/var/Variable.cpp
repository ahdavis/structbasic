/*
 * Variable.cpp
 * Implements a class that represents a variable
 * Created on 2/14/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "Variable.h"

//default constructor
Variable::Variable()
	: Variable("NoName") //call the name-only constructor
{
	//no code needed
}

//name-only constructor
Variable::Variable(const std::string& newName)
	: Variable(newName, 0) //call the main constructor
{
	//no code needed
}

//main constructor
Variable::Variable(const std::string& newName, const Variant& newValue)
	: name(newName), value(newValue) //init the fields
{
	//no code needed
}

//destructor
Variable::~Variable() {
	//no code needed
}

//copy constructor
Variable::Variable(const Variable& v)
	: name(v.name), value(v.value) //copy the fields
{
	//no code needed
}

//move constructor
Variable::Variable(Variable&& v)
	: name(v.name), value(v.value) //move the fields
{
	//no code needed
}

//assignment operator
Variable& Variable::operator=(const Variable& src) {
	this->name = src.name; //assign the name field
	this->value = src.value; //assign the value field
	return *this; //and return the instance
}

//move operator
Variable& Variable::operator=(Variable&& src) {
	this->name = src.name; //move the name field
	this->value = src.value; //move the value field
	return *this; //and return the instance
}

//equality operator
bool Variable::operator==(const Variable& other) const {
	return this->value == other.value; //compare the values
}

//inequality operator
bool Variable::operator!=(const Variable& other) const {
	return this->value != other.value; //compare the values
}

//less-than operator
bool Variable::operator<(const Variable& other) const {
	return this->value < other.value; //compare the values
}

//greater-than operator
bool Variable::operator>(const Variable& other) const {
	return this->value > other.value; //compare the values
}

//less-than-or-equal-to operator
bool Variable::operator<=(const Variable& other) const {
	return this->value <= other.value; //compare the values
}

//greater-than-or-equal-to operator
bool Variable::operator>=(const Variable& other) const {
	return this->value >= other.value; //compare the values
}

//autoincrement operator
Variable& Variable::operator++() {
	++(this->value); //increment the value
	return *this; //and return the instance
}

//autodecrement operator
Variable& Variable::operator--() {
	--(this->value); //decrement the value
	return *this; //and return the instance
}

//negation operator
Variable Variable::operator-() {
	Variable v; //the return type

	//negate the variable
	v.value = -(this->value);

	return v; //and return the negated variable
}

//addition operator
Variable& Variable::operator+=(const Variable& other) {
	this->value += other.value; //add the other variable
	return *this; //and return the instance
}

//subtraction operator
Variable& Variable::operator-=(const Variable& other) {
	this->value -= other.value; //subtract the other variable
	return *this; //and return the instance
}

//modulus operator
Variable& Variable::operator%=(const Variable& other) {
	this->value %= other.value; //mod the other variable
	return *this; //and return the instance
}

//multiplication operator
Variable& Variable::operator*=(const Variable& other) {
	this->value *= other.value; //multiply the other variable
	return *this; //and return the instance
}

//division operator
Variable& Variable::operator/=(const Variable& other) {
	this->value /= other.value; //divide by the other variable
	return *this; //and return the instance
}


//getName method - returns the name of the Variable
const std::string& Variable::getName() const {
	return this->name; //return the name field
}

//getValue method - returns the value of the Variable
const Variant& Variable::getValue() const {
	return this->value; //return the value field
}

//setName method - sets the name of the Variable
void Variable::setName(const std::string& newName) {
	this->name = newName; //set the name field
}

//setValue method - sets the value of the Variable
void Variable::setValue(const Variant& newValue) {
	this->value = newValue; //set the value field
}

//output operator
std::ostream& operator<<(std::ostream& os, const Variable& v) {
	os << v.value; //stream out the variable's value
	return os; //and return the stream
}

//input operator
std::istream& operator>>(std::istream& is, Variable& v) {
	is >> v.value; //stream into the variable's value
	return is; //and return the stream
}

//overloaded getline function
void getline(std::istream& is, Variable& v) {
	getline(is, v.value); //read into the Variant field
}

//end of implementation
