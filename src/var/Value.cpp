/*
 * Value.cpp
 * Implements a union that can assume any primitive type in StructBASIC
 * Created on 1/30/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "Value.h"

//integer constructor
Value::Value(int newValue)
	: i(newValue) //init the int field
{
	//no code needed
}

//float constructor
Value::Value(double newValue)
	: d(newValue) //init the double field
{
	//no code needed
}

//char constructor
Value::Value(char newValue)
	: c(newValue) //init the char field
{
	//no code needed
}

//boolean constructor
Value::Value(bool newValue)
	: b(newValue) //init the boolean field
{
	//no code needed
}

//string constructor
Value::Value(const char* newValue)
	: s(nullptr) //null the string field
{
	//copy the string
	this->s = dupstr(newValue); 
}

//destructor
Value::~Value() {
	//no code needed
}

//free method - deallocates the union
void Value::free() {
	std::free(this->s); //deallocate the string
	this->s = NULL; //and zero it out
}

//end of implementation
