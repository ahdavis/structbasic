/*
 * Variable.h
 * Declares a class that represents a variable
 * Created on 2/14/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <string>
#include <iostream>
#include "Variant.h"

//class declaration
class Variable final {
	//public fields and methods
	public:
		//default constructor
		Variable();

		//name-only constructor
		explicit Variable(const std::string& newName);

		//main constructor
		Variable(const std::string& newName, 
				const Variant& newValue);

		//destructor
		~Variable();

		//copy constructor
		Variable(const Variable& v);

		//move constructor
		Variable(Variable&& v);

		//assignment operator
		Variable& operator=(const Variable& src);

		//move operator
		Variable& operator=(Variable&& src);

		//overloaded operators
		
		//equality operator
		bool operator==(const Variable& other) const;

		//inequality operator
		bool operator!=(const Variable& other) const;

		//less-than operator
		bool operator<(const Variable& other) const;

		//greater-than operator
		bool operator>(const Variable& other) const;

		//less-than-or-equal-to operator
		bool operator<=(const Variable& other) const;

		//greater-than-or-equal-to operator
		bool operator>=(const Variable& other) const;

		//autoincrement operator
		Variable& operator++();

		//autodecrement operator
		Variable& operator--();

		//negation operator
		Variable operator-();
		
		//addition operator
		Variable& operator+=(const Variable& other);

		//subtraction operator
		Variable& operator-=(const Variable& other);

		//modulus operator
		Variable& operator%=(const Variable& other);

		//multiplication operator
		Variable& operator*=(const Variable& other);

		//division operator
		Variable& operator/=(const Variable& other);

		//getter methods
		
		//returns the name of the Variable
		const std::string& getName() const;

		//returns the value of the Variable
		const Variant& getValue() const;

		//setter methods
		
		//sets the name of the Variable
		void setName(const std::string& newName);
		
		//sets the value of the Variable
		void setValue(const Variant& newValue);

		//I/O operators
		
		//output operator
		friend std::ostream& operator<<(std::ostream& os,
						const Variable& v);

		//input operator
		friend std::istream& operator>>(std::istream& is,
							Variable& v);

		//overloaded getline function
		friend void getline(std::istream& is, Variable& v);

	//private fields and methods
	private:
		//fields
		std::string name; //the name of the Variable
		Variant value; //the value of the Variable
};

//end of header
