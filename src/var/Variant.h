/*
 * Variant.h
 * Declares a class that can assume any primitive type in StructBASIC
 * Created on 1/30/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <string>
#include <iostream>
#include <sstream>
#include <cctype>
#include <cmath>
#include "VarType.h"
#include "Value.h"
#include "../except/TypeException.h"
#include "../except/OpException.h"
#include "../util/dupstr.h"
#include "../util/typecheck.h"

//class declaration
class Variant final {
	//public fields and methods
	public:
		//default constructor
		Variant();

		//integer constructor
		Variant(int newValue);

		//double constructor
		Variant(double newValue);

		//char constructor
		Variant(char newValue);

		//bool constructor
		Variant(bool newValue);

		//C string constructor
		Variant(const char* newValue);

		//std::string constructor
		Variant(const std::string& newValue);

		//destructor
		~Variant();

		//copy constructor
		Variant(const Variant& v);

		//move constructor
		Variant(Variant&& v);

		//default assignment operator
		Variant& operator=(const Variant& src);

		//move operator
		Variant& operator=(Variant&& src);

		//integer assignment operator
		Variant& operator=(int newValue);

		//double assignment operator
		Variant& operator=(double newValue);

		//char assignment operator
		Variant& operator=(char newValue);

		//bool assignment operator
		Variant& operator=(bool newValue);

		//C string assignment operator
		Variant& operator=(const char* newValue);

		//std::string assignment operator
		Variant& operator=(const std::string& newValue);

		//comparison operators
		//Comparison operators will throw an OpException
		//if the types are not compatible with the comparison

		//equality operator
		bool operator==(const Variant& other) const;

		//inequality operator
		bool operator!=(const Variant& other) const;

		//less-than operator
		bool operator<(const Variant& other) const;

		//greater-than operator
		bool operator>(const Variant& other) const;

		//less-than-or-equal-to operator
		bool operator<=(const Variant& other) const;

		//greater-than-or-equal-to operator
		bool operator>=(const Variant& other) const;

		//math operators
		//Math operators will throw an OpException if
		//the types are not compatible with the operation

		//autoincrement operator
		Variant& operator++();

		//autodecrement operator
		Variant& operator--();

		//unary negation operator
		Variant operator-();

		//addition operator
		Variant& operator+=(const Variant& other);

		//subtraction operator
		Variant& operator-=(const Variant& other);

		//multiplication operator
		Variant& operator*=(const Variant& other);

		//division operator
		Variant& operator/=(const Variant& other);

		//modulus operator
		Variant& operator%=(const Variant& other);

		//getter methods
		
		//If the Variant is not currently storing
		//the type associated with the called 
		//getter method, a TypeException will
		//be thrown
		
		//returns the Variant as an integer
		int intValue() const;

		//returns the Variant as a double
		double doubleValue() const;

		//returns the Variant as a character
		char charValue() const;

		//returns the Variant as a boolean
		bool boolValue() const;

		//returns the Variant as a string
		std::string stringValue() const;

		//returns the type being held in the Variant
		VarType getType() const;

		//output operator
		friend std::ostream& operator<<(std::ostream& os, 
							const Variant& v);

		//input operator
		friend std::istream& operator>>(std::istream& is,
							Variant& v);

		//getline function
		friend void getline(std::istream& is, Variant& v);

	//private fields and methods
	private:
		//method
		void free(); //deallocates the Variant

		//fields
		VarType type; //the current type being stored
		Value value; //the current value being stored		
};

//end of header
