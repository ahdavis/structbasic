/*
 * VarRegistry.cpp
 * Implements a singleton class that stores variables
 * Created on 2/14/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "VarRegistry.h"

//private constructor
VarRegistry::VarRegistry()
	: db() //init the field
{
	//no code needed
}

//destructor
VarRegistry::~VarRegistry() {
	this->db.clear(); //clear the registry
}

//static getInstance method - returns an instance of the class
VarRegistry& VarRegistry::getInstance() {
	static VarRegistry ret; //create an instance of the class
	return ret; //and return it
}

//lookup method - retrieves a variable from the database
const Variable& VarRegistry::lookup(const std::string& name) const {
	//make sure the variable exists in the database
	if(this->hasVariable(name)) { //if the variable exists
		return this->db.at(name); //then return it
	} else { //if the variable does not exist
		throw UndefVarException(name); //then throw an exception
	}
}

//hasVariable method - returns whether a variable exists in the registry
bool VarRegistry::hasVariable(const std::string& name) const {
	//get an iterator to check for the variable name
	auto it = this->db.find(name);

	//and return whether the iterator found the name
	return it != this->db.end();
}

//store method - stores a variable in the database
void VarRegistry::store(const Variable& var) {
	this->db[var.getName()] = var; //store the variable
}

//end of implementation
