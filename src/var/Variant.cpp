/*
 * Variant.cpp
 * Implements a class that can assume any primitive type in StructBASIC
 * Created on 1/30/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "Variant.h"

//default constructor
Variant::Variant()
	: Variant(0) //call the float constructor
{
	//no code needed
}

//integer constructor
Variant::Variant(int newValue)
	: type(VarType::INT), value(newValue) //init the fields
{
	//no code needed
}

//double constructor
Variant::Variant(double newValue)
	: type(VarType::DOUBLE), value(newValue) //init the fields
{
	//no code needed
}

//char constructor
Variant::Variant(char newValue)
	: type(VarType::CHAR), value(newValue) //init the fields
{
	//no code needed
}

//boolean constructor
Variant::Variant(bool newValue)
	: type(VarType::BOOL), value(newValue) //init the fields
{
	//no code needed
}

//C string constructor
Variant::Variant(const char* newValue)
	: type(VarType::STRING), value(newValue) //init the fields
{
	//no code needed
}

//std::string constructor
Variant::Variant(const std::string& newValue)
	: Variant(newValue.c_str()) //call the C string constructor
{
	//no code needed
}

//destructor
Variant::~Variant() {
	this->free(); //deallocate the variant
}

//copy constructor
Variant::Variant(const Variant& v)
	: type(v.type), value(0) //copy the fields
{
	//switch on the type
	switch(this->type) {
		case VarType::INT: //integer
			{
				this->value.i = v.value.i; //init the int
				break;
			}
		case VarType::DOUBLE: //float
			{
				this->value.d = v.value.d; //init the float
				break;
			}
		case VarType::CHAR: //character
			{
				this->value.c = v.value.c; //init the char
				break;
			}
		case VarType::BOOL: //boolean
			{
				this->value.b = v.value.b; //init the bool
				break;
			}
		case VarType::STRING: //string
			{
				//init the string
				this->value.s = dupstr(v.value.s);
				break;
			}
		default: //unknown type
			{
				break;
			}
	}
}

//move constructor
Variant::Variant(Variant&& v)
	: type(v.type), value(0) //move the fields
{
	//switch on the type
	switch(this->type) {
		case VarType::INT: //integer
			{
				this->value.i = v.value.i; //init the int
				break;
			}
		case VarType::DOUBLE: //float
			{
				this->value.d = v.value.d; //init the float
				break;
			}
		case VarType::CHAR: //character
			{
				this->value.c = v.value.c; //init the char
				break;
			}
		case VarType::BOOL: //boolean
			{
				this->value.b = v.value.b; //init the bool
				break;
			}
		case VarType::STRING: //string
			{
				//init the string
				this->value.s = dupstr(v.value.s);
				break;
			}
		default: //unknown type
			{
				break;
			}
	}

	//free the other variant
	v.free();
}

//assignment operator
Variant& Variant::operator=(const Variant& src) {
	//free the current variant
	this->free();

	//assign the type
	this->type = src.type;

	//switch on the type
	switch(this->type) {
		case VarType::INT: //integer
			{
				this->value.i = src.value.i; //assign int
				break;
			}
		case VarType::DOUBLE: //float
			{
				this->value.d = src.value.d; //float
				break;
			}
		case VarType::CHAR: //character
			{
				this->value.c = src.value.c; //assign char
				break;
			}
		case VarType::BOOL: //boolean
			{
				this->value.b = src.value.b; //assign bool
				break;
			}
		case VarType::STRING: //string
			{
				//assign the string
				this->value.s = dupstr(src.value.s);
				break;
			}
		default: //unknown type
			{
				this->value.b = false;
			}
	}

	//and return the instance
	return *this;
}

//move operator
Variant& Variant::operator=(Variant&& src) {
	//free the current variant
	this->free();

	//move the type
	this->type = src.type;

	//switch on the type
	switch(this->type) {
		case VarType::INT: //integer
			{
				this->value.i = src.value.i; //move int
				break;
			}
		case VarType::DOUBLE: //float
			{
				this->value.d = src.value.d; //float
				break;
			}
		case VarType::CHAR: //character
			{
				this->value.c = src.value.c; //move char
				break;
			}
		case VarType::BOOL: //boolean
			{
				this->value.b = src.value.b; //move bool
				break;
			}
		case VarType::STRING: //string
			{
				//move the string
				this->value.s = dupstr(src.value.s);
				break;
			}
		default: //unknown type
			{
				this->value.b = false;
			}
	}

	//free the temporary
	src.free();

	//and return the instance
	return *this;
}

//integer assignment operator
Variant& Variant::operator=(int newValue) {
	//free the current value
	this->free();

	//change the type to integer
	this->type = VarType::INT;

	//change the value
	this->value.i = newValue; 

	//and return the instance
	return *this;
}

//double assignment operator
Variant& Variant::operator=(double newValue) {
	//free the current value
	this->free();

	//change the type to float
	this->type = VarType::DOUBLE;

	//change the value
	this->value.d = newValue; 

	//and return the instance
	return *this;
}

//char assignment operator
Variant& Variant::operator=(char newValue) {
	//free the current value
	this->free();

	//change the type to char
	this->type = VarType::CHAR;

	//change the value
	this->value.c = newValue; 

	//and return the instance
	return *this;
}

//bool assignment operator
Variant& Variant::operator=(bool newValue) {
	//free the current value
	this->free();

	//change the type to bool
	this->type = VarType::BOOL;

	//change the value
	this->value.b = newValue;

	//and return the instance
	return *this;
}

//C string assignment operator
Variant& Variant::operator=(const char* newValue) {
	//free the value
	this->free();

	//change the type to string
	this->type = VarType::STRING;

	//change the value
	this->value.s = dupstr(newValue);

	//and return the instance
	return *this;
}

//std::string assignment operator
Variant& Variant::operator=(const std::string& newValue) {
	Variant::operator=(newValue.c_str()); //call the other operator
	return *this; //and return the instance
}

//equality operator
bool Variant::operator==(const Variant& other) const {
	//make sure the Variants hold the same type
	if(this->type != other.type) { //if the types don't match
		//then throw an exception
		throw OpException(this->type, other.type);
	}

	//switch on the type
	switch(this->type) {
		case VarType::BOOL: //boolean types
			{
				//return a comparison
				return (this->value.b == other.value.b);
			}
		case VarType::CHAR: //character types
			{
				//return a comparison
				return (this->value.c == other.value.c);
			}
		case VarType::DOUBLE: //float types
			{
				//get the difference of the variants
				float dif = this->value.d - other.value.d;

				//get the machine epsilon value
				float epsilon = std::numeric_limits<float>
					::epsilon();
				
				//and return whether the values are close
				return (fabs(dif) < epsilon); 
			}
		case VarType::INT: //int types
			{
				//return a comparison
				return (this->value.i == other.value.i);
			}
		case VarType::STRING: //string types
			{
				//get the values as string objects
				std::string thisVal = 
					this->stringValue();
				std::string otherVal =
					other.stringValue();

				//and return their comparison
				return thisVal == otherVal;
			}
		default: //unknown types
			{
				return false;
			}
	}

}

//inequality operator
bool Variant::operator!=(const Variant& other) const {
	//get the equality value of the variants
	bool equals = (*this == other);

	//and return its negative
	return !equals;
}

//less-than operator
bool Variant::operator<(const Variant& other) const {
	//make sure the variants are the same type
	if(this->type != other.type) { //if the types don't match
		//then throw an exception
		throw OpException(this->type, other.type);
	}

	//switch on the type
	switch(this->type) {
		case VarType::INT: //integer
			{
				//return the comparison
				return this->value.i < other.value.i;
			}
		case VarType::CHAR: //character
			{
				//return the comparison
				return this->value.c < other.value.c;
			}
		case VarType::DOUBLE: //float
			{
				//return the comparison
				return this->value.d < other.value.d;
			}
		case VarType::STRING: //string
			{
				//return the comparison
				return this->stringValue() < 
					other.stringValue();
			}
		default: //other types
			{
				//throw an exception
				throw OpException(this->type, other.type);
			}
	}
}

//greater-than operator
bool Variant::operator>(const Variant& other) const {
	//make sure the variants are the same type
	if(this->type != other.type) { //if the types don't match
		//then throw an exception
		throw OpException(this->type, other.type);
	}

	//switch on the type
	switch(this->type) {
		case VarType::INT: //integer
			{
				//return the comparison
				return this->value.i > other.value.i;
			}
		case VarType::CHAR: //character
			{
				//return the comparison
				return this->value.c > other.value.c;
			}
		case VarType::DOUBLE: //float
			{
				//return the comparison
				return this->value.d > other.value.d;
			}
		case VarType::STRING: //string
			{
				//return the comparison
				return this->stringValue() >
					other.stringValue();
			}
		default: //other types
			{
				//throw an exception
				throw OpException(this->type, other.type);
			}
	}
}

//less-than-or-equal-to operator
bool Variant::operator<=(const Variant& other) const {
	//make sure the variants are the same type
	if(this->type != other.type) { //if the types don't match
		//then throw an exception
		throw OpException(this->type, other.type);
	}

	//switch on the type
	switch(this->type) {
		case VarType::INT: //integer
			{
				//return the comparison
				return this->value.i <= other.value.i;
			}
		case VarType::CHAR: //character
			{
				//return the comparison
				return this->value.c <= other.value.c;
			}
		case VarType::DOUBLE: //float
			{
				//return the comparison
				return this->value.d <= other.value.d;
			}
		case VarType::STRING: //string
			{
				//return the comparison
				return this->stringValue() <=
					other.stringValue();
			}
		default: //other types
			{
				//throw an exception
				throw OpException(this->type, other.type);
			}
	}
}

//greater-than-or-equal-to operator
bool Variant::operator>=(const Variant& other) const {
	//make sure the variants are the same type
	if(this->type != other.type) { //if the types don't match
		//then throw an exception
		throw OpException(this->type, other.type);
	}

	//switch on the type
	switch(this->type) {
		case VarType::INT: //integer
			{
				//return the comparison
				return this->value.i >= other.value.i;
			}
		case VarType::CHAR: //character
			{
				//return the comparison
				return this->value.c >= other.value.c;
			}
		case VarType::DOUBLE: //float
			{
				//return the comparison
				return this->value.d >= other.value.d;
			}
		case VarType::STRING: //string
			{
				//return the comparison
				return this->stringValue() >=
					other.stringValue();
			}
		default: //other types
			{
				//throw an exception
				throw OpException(this->type, other.type);
			}
	}
}

//autoincrement operator
Variant& Variant::operator++() {
	//verify that the types are valid
	if((this->type != VarType::CHAR) && (this->type != VarType::INT)) {
		//then throw an exception
		throw OpException(this->type);
	}

	//determine which union field to use
	if(this->type == VarType::CHAR) {
		this->value.c++; //increment the char field
	} else {
		this->value.i++; //increment the int field
	}

	return *this; //and return the instance
}

//autodecrement operator
Variant& Variant::operator--() {
	//verify that the types are valid
	if((this->type != VarType::CHAR) && (this->type != VarType::INT)) {
		//then throw an exception
		throw OpException(this->type);
	}

	//determine which union field to use
	if(this->type == VarType::CHAR) {
		this->value.c--; //increment the char field
	} else {
		this->value.i--; //increment the int field
	}

	return *this; //and return the instance
}

//unary negation operator
Variant Variant::operator-() {
	//verify that the types are valid
	if((this->type != VarType::DOUBLE) && (this->type != VarType::INT)		&& (this->type != VarType::BOOL)) {
		//throw an exception
		throw OpException(this->type);
	}

	//declare the return type
	Variant v;

	//determine which union field to use
	if(this->type == VarType::DOUBLE) {
		v = -(this->value.d); //negate the float field
	} else if(this->type == VarType::BOOL) {
		v = !(this->value.b); //negate the bool field
	} else {
		v = -(this->value.i); //negate the int field
	}

	//and return the negated value
	return v;
}

//addition operator
Variant& Variant::operator+=(const Variant& other) {
	//switch on the type
	switch(this->type) {
		case VarType::BOOL: //boolean type
			{
				//make sure that both values are booleans
				if((this->type != VarType::BOOL) ||
					(other.type != VarType::BOOL)) {
					throw OpException(this->type,
							other.type);
				}

				//evaluate the operation
				*this = (this->value.b || other.value.b);

				break;
			}
		case VarType::CHAR: //character type
			{
				//evaluate the operation
				*this = (this->value.c + other.value.c);

				break;
			}
		case VarType::DOUBLE: //float type
			{
				if(other.type == VarType::DOUBLE) {
					//evaluate the operation
					*this = (this->value.d + 
							other.value.d);
				} else if(other.type == VarType::INT) {
					//evaluate the operation
					*this = (this->value.d +
							other.value.i);
				} else {
					//throw an exception
					throw OpException(this->type,
							other.type);
				}

				break;
			}
		case VarType::INT: //int type
			{
				if(other.type == VarType::DOUBLE) {
					//evaluate the operation
					*this = (this->value.i + 
							other.value.d);
				} else if(other.type == VarType::INT) {
					//evaluate the operation
					*this = (this->value.i +
							other.value.i);
				} else {
					//throw an exception
					throw OpException(this->type,
							other.type);
				}
	
				break;
			}
		case VarType::STRING: //string type
			{			
				//evaluate the operation
				*this = (this->stringValue() + 
						other.stringValue());

				break;
			}
		default: //unknown type
			{
				//throw an exception
				throw OpException(this->type, other.type);
			}
	}

	//and return the instance
	return *this;
}

//subtraction operator
Variant& Variant::operator-=(const Variant& other) {
	//switch on the type
	switch(this->type) {
		case VarType::BOOL: //boolean type
			{
				//throw an exception
				throw OpException(this->type, other.type);
				break;
			}
		case VarType::CHAR: //character type
			{
				//evaluate the operation
				*this = (this->value.c - other.value.c);

				break;
			}
		case VarType::DOUBLE: //float type
			{
				if(other.type == VarType::DOUBLE) {
					//evaluate the operation
					*this = (this->value.d -
							other.value.d);
				} else if(other.type == VarType::INT) {
					//evaluate the operation
					*this = (this->value.d -
							other.value.i);
				} else {
					//throw an exception
					throw OpException(this->type,
							other.type);
				}

				break;
			}
		case VarType::INT: //int type
			{
				if(other.type == VarType::DOUBLE) {
					//evaluate the operation
					*this = (this->value.i - 
							other.value.d);
				} else if(other.type == VarType::INT) {
					//evaluate the operation
					*this = (this->value.i -
							other.value.i);
				} else {
					//throw an exception
					throw OpException(this->type,
							other.type);
				}
	
				break;
			}
		case VarType::STRING: //string type
			{			
				//throw an exception
				throw OpException(this->type, other.type);

				break;
			}
		default: //unknown type
			{
				//throw an exception
				throw OpException(this->type, other.type);
			}
	}

	//and return the instance
	return *this;
}

//multiplication operator
Variant& Variant::operator*=(const Variant& other) {
	//switch on the type
	switch(this->type) {
		case VarType::BOOL: //boolean type
			{
				//make sure both operands are booleans
				if((this->type != VarType::BOOL) ||
					(other.type != VarType::BOOL)) {
					throw OpException(this->type,
							other.type);
				}

				//evaluate the operation
				*this = (this->value.b && other.value.b);

				break;
			}
		case VarType::CHAR: //character type
			{
				//check to see if the other operand
				//is an int
				if(other.type != VarType::INT) {
					throw OpException(this->type,
							other.type);
				}

				//assemble the string
				std::stringstream ss;

				//loop and assemble the string
				for(int i = 0; i < other.intValue(); i++) {
					ss << this->charValue();
				}

				//assign the assembled string
				*this = ss.str();

				break;
			}
		case VarType::DOUBLE: //float type
			{
				if(other.type == VarType::DOUBLE) {
					//evaluate the operation
					*this = (this->value.d *
							other.value.d);
				} else if(other.type == VarType::INT) {
					//evaluate the operation
					*this = (this->value.d *
							other.value.i);
				} else {
					//throw an exception
					throw OpException(this->type,
							other.type);
				}


				break;
			}
		case VarType::INT: //int type
			{
				if(other.type == VarType::DOUBLE) {
					//evaluate the operation
					*this = (this->value.i *
							other.value.d);
				} else if(other.type == VarType::INT) {
					//evaluate the operation
					*this = (this->value.i *
							other.value.i);
				} else if(other.type == VarType::CHAR) {
					//assemble the string
					std::stringstream ss;

					//loop and assemble the string
					for(int i = 0; 
						i < this->intValue();
						i++) {
						ss << other.charValue();
					}

					//and assign the assembled string
					*this = ss.str();
				} else {
					//throw an exception
					throw OpException(this->type,
							other.type);
				}

				break;
			}
		case VarType::STRING: //string type
			{			
				//throw an exception
				throw OpException(this->type, other.type);

				break;
			}
		default: //unknown type
			{
				//throw an exception
				throw OpException(this->type, other.type);
			}
	}

	//and return the instance
	return *this;
}

//division operator
Variant& Variant::operator/=(const Variant& other) {
	//switch on the type
	switch(this->type) {
		case VarType::BOOL: //boolean type
			{
				//throw an exception
				throw OpException(this->type, other.type);
			}
		case VarType::CHAR: //character type
			{

				//throw an exception
				throw OpException(this->type, other.type);
			}
		case VarType::DOUBLE: //float type
			{
				if(other.type == VarType::DOUBLE) {
					//evaluate the operation
					*this = (this->value.d / 
							other.value.d);
				} else if(other.type == VarType::INT) {
					//evaluate the operation
					*this = (this->value.d / 
							other.value.i);
				} else {
					//throw an exception
					throw OpException(this->type,
							other.type);
				}

				break;
			}
		case VarType::INT: //int type
			{
				if(other.type == VarType::DOUBLE) {
					//evaluate the operation
					*this = (this->value.i / 
							other.value.d);
				} else if(other.type == VarType::INT) {
					//evaluate the operation
					*this = (this->value.i / 
							other.value.i);
				} else {
					//throw an exception
					throw OpException(this->type,
							other.type);
				}

				break;
			}
		case VarType::STRING: //string type
			{			
				//throw an exception
				throw OpException(this->type, other.type);

				break;
			}
		default: //unknown type
			{
				//throw an exception
				throw OpException(this->type, other.type);
			}
	}

	//and return the instance
	return *this;
}

//modulus operator
Variant& Variant::operator%=(const Variant& other) {
	//make sure the types match
	if(this->type != other.type) {
		//throw an exception
		throw OpException(this->type, other.type);
	}

	//switch on the type
	switch(this->type) {
		case VarType::BOOL: //boolean type
			{
				//throw an exception
				throw OpException(this->type, other.type);
			}
		case VarType::CHAR: //character type
			{

				//throw an exception
				throw OpException(this->type, other.type);
			}
		case VarType::DOUBLE: //float type
			{
				//evaluate the operation
				*this = fmod(this->value.d, other.value.d);
				
				break;
			}
		case VarType::INT: //int type
			{
				//evaluate the operation
				*this = (this->value.i % other.value.i);

				break;
			}
		case VarType::STRING: //string type
			{			
				//throw an exception
				throw OpException(this->type, other.type);

				break;
			}
		default: //unknown type
			{
				//throw an exception
				throw OpException(this->type, other.type);
			}
	}

	//and return the instance
	return *this;
}

//intValue method - returns the Variant as an integer
int Variant::intValue() const {
	//make sure that the Variant is an int
	if(this->type != VarType::INT) {
		//throw a type exception
		throw TypeException(VarType::INT, this->type);
	}

	//return the Variant's int value
	return this->value.i;
}

//doubleValue method - returns the Variant as a double
double Variant::doubleValue() const {
	//make sure that the Variant is a float
	if(this->type != VarType::DOUBLE) {
		//throw a type exception
		throw TypeException(VarType::DOUBLE, this->type);
	}

	//return the Variant's double value 
	return this->value.d;
}

//charValue method - returns the Variant as a char
char Variant::charValue() const {
	//make sure that the Variant is a char
	if(this->type != VarType::CHAR) {
		//throw a type exception
		throw TypeException(VarType::CHAR, this->type);
	}

	//return the Variant's char value 
	return this->value.c;
}

//boolValue method - returns the Variant as a bool
bool Variant::boolValue() const {
	//make sure that the Variant is a bool
	if(this->type != VarType::BOOL) {
		//throw a type exception
		throw TypeException(VarType::BOOL, this->type);
	}

	//return the Variant's bool value 
	return this->value.b;
}

//stringValue method - returns the Variant as a string
std::string Variant::stringValue() const {
	//make sure that the Variant is a string
	if(this->type != VarType::STRING) {
		//throw a type exception
		throw TypeException(VarType::STRING, this->type);
	}

	//return the Variant's string value 
	return std::string(this->value.s);
}

//getType method - returns the type being held in the Variant
VarType Variant::getType() const {
	return this->type; //return the type field
}

//output operator
std::ostream& operator<<(std::ostream& os, const Variant& v) {
	//switch on the type
	switch(v.type) {
		case VarType::BOOL: //boolean
			{
				//handle true and false values
				if(v.value.b) { //true
					os << "true"; //stream out true
				} else { //false
					os << "false"; //stream out false
				}

				break;
			}
		case VarType::CHAR: //character
			{
				os << v.value.c; //stream out the character
				break;
			}
		case VarType::DOUBLE: //float
			{
				os << v.value.d; //stream out the float
				break;
			}
		case VarType::INT: //integer
			{
				os << v.value.i; //stream out the integer
				break;
			}
		case VarType::STRING: //string
			{
				os << v.stringValue(); //stream the string
				break;
			}
		default: //unknown type
			{
				os << "Error: Unknown type";
				break;
			}
	}
	
	//return the stream
	return os;
}

//input operator
std::istream& operator>>(std::istream& is, Variant& v) {
	//read in the input
	std::string input;
	is >> input;

	//determine the type of the input
	if(checkInt(input)) { //if the input is an integer
		v = std::stoi(input); //assign the value as an int
	} else if(checkFloat(input)) { //if the input is a double
		v = std::stod(input); //then assign the value as a double
	} else if(checkChar(input)) { //if the input is a character
		v = input[0]; //then assign the value as a character
	} else if(checkBool(input)) { //if the input is a boolean
		v = (input == "true"); //then assign the value as a boolean
	} else { //if the input is a string
		v = input; //then assign the value as a string
	}

	//and return the stream
	return is;
}

//overloaded getline function
void getline(std::istream& is, Variant& v) {
	std::string temp; //the string to read in
	char c; //the character being read in

	is.get(c); //read in the first character

	//loop and read in the string
	while(c != '\n' && !is.eof()) {
		temp += c;
		is.get(c);
	}
	//determine the type of the input
	if(checkInt(temp)) { //if the input is an integer
		v = std::stoi(temp); //assign the value as an int
	} else if(checkFloat(temp)) { //if the input is a double
		v = std::stod(temp); //then assign the value as a double
	} else if(checkChar(temp)) { //if the input is a character
		v = temp[0]; //then assign the value as a character
	} else if(checkBool(temp)) { //if the input is a boolean
		v = (temp == "true"); //then assign the value as a boolean
	} else { //if the input is a string
		v = temp; //then assign the value as a string
	}


}
//private free method - deallocates the variant
void Variant::free() {
	//make sure the value is a string
	if(this->type == VarType::STRING) {
		this->value.free(); //free the union instance
	}
}

//end of implementation
