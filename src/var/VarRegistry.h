/*
 * VarRegistry.h
 * Declares a singleton class that stores variables
 * Created on 2/14/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <string>
#include <map>
#include "Variable.h"
#include "../except/UndefVarException.h"

//class declaration
class VarRegistry final {
	//public fields and methods
	public:
		//constructor is private
		
		//destructor
		~VarRegistry();

		//copy and move constructors are deleted
		VarRegistry(const VarRegistry& var) = delete;
		VarRegistry(VarRegistry&& var) = delete;

		//assignment and move operators are deleted
		VarRegistry& operator=(const VarRegistry& src) = delete;
		VarRegistry& operator=(VarRegistry&& src) = delete;

		//returns an instance of the class
		static VarRegistry& getInstance();

		//getter methods
		
		//returns a variable referenced by a name
		const Variable& lookup(const std::string& name) const;

		//returns whether a variable is in the registry
		bool hasVariable(const std::string& name) const; 

		//setter method
		
		//stores a variable in the registry
		void store(const Variable& var);

	//private fields and methods
	private:
		//constructor
		VarRegistry();

		//field
		std::map<std::string, Variable> db; //the registry database
};

//end of header
