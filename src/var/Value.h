/*
 * Value.h
 * Declares a union that can assume any primitive type in StructBASIC
 * Created on 1/30/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../util/dupstr.h"
#include <cstdlib>

//NOTE: The union does not automatically deallocate its string value.
//Value::free() must be called to do that.

//union declaration
union Value {
	//integer constructor
	Value(int newValue);

	//double constructor
	Value(double newValue);

	//char constructor
	Value(char newValue);

	//boolean constructor
	Value(bool newValue);

	//string constructor
	Value(const char* newValue);

	//destructor
	~Value();

	//deallocates the union instance
	//MUST BE CALLED WHEN A UNION INSTANCE IS DESTRUCTED
	void free();

	//fields
	int i; //the int instance
	double d; //the double instance
	char c; //the character instance
	bool b; //the boolean instance
	char* s; //the string instance
};

//end of header
