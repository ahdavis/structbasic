/*
 * SourceException.cpp
 * Implements an exception that is thrown when reading source code fails
 * Created on 2/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "SourceException.h"

//constructor
SourceException::SourceException(const std::string& fileName)
	: errMsg() //init the field
{
	//assemble the error message
	this->errMsg = "error reading from source file " + fileName;
}

//destructor
SourceException::~SourceException() {
	//no code needed
}

//copy constructor
SourceException::SourceException(const SourceException& se)
	: errMsg(se.errMsg) //copy the field
{
	//no code needed
}

//move constructor
SourceException::SourceException(SourceException&& se)
	: errMsg(se.errMsg) //move the field
{
	//no code needed
}

//assignment operator
SourceException& SourceException::operator=(const SourceException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//move operator
SourceException& SourceException::operator=(SourceException&& src) {
	this->errMsg = src.errMsg; //move the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* SourceException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
