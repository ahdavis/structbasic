/*
 * ParserException.cpp
 * Implements an exception that is thrown when the lexer finds an error
 * Created on 2/5/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "ParserException.h"

//constructor
ParserException::ParserException()
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "(" << curLineNo << ":" << curCharNo << ") "
	   << "error: syntax error";
	this->errMsg = ss.str();
}

//destructor
ParserException::~ParserException() {
	//no code needed
}

//assignment operator
ParserException::ParserException(const ParserException& pe)
	: errMsg(pe.errMsg) //copy the error message
{
	//no code needed
}

//move operator
ParserException::ParserException(ParserException&& pe)
	: errMsg(pe.errMsg) //move the error message
{
	//no code needed
}

//assignment operator
ParserException& ParserException::operator=(const ParserException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//move operator
ParserException& ParserException::operator=(ParserException&& src) {
	this->errMsg = src.errMsg; //move the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* ParserException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
