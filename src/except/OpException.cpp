/*
 * OpException.cpp
 * Implements an exception that is thrown when an invalid operation is found
 * Created on 2/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "OpException.h"

//first constructor
OpException::OpException(VarType badType)
	: errMsg() //init the field
{
	//assemble the error message
	std::stringstream ss; 
	ss << "error: operation not supported for type " 
	   << typeName(badType);
	this->errMsg = ss.str();
}

//second constructor
OpException::OpException(VarType badType1, VarType badType2)
	: errMsg() //init the field
{
	//assemble the error message
	std::stringstream ss; 
	ss << "error: operation not supported for types " 
	   << typeName(badType1) <<
	   " and " 
	   << typeName(badType2);
	this->errMsg = ss.str();
}

//destructor
OpException::~OpException() {
	//no code needed
}

//copy constructor
OpException::OpException(const OpException& oe)
	: errMsg(oe.errMsg) //copy the message
{
	//no code needed
}

//move constructor
OpException::OpException(OpException&& oe)
	: errMsg(oe.errMsg) //move the message
{
	//no code needed
}

//assignment operator
OpException& OpException::operator=(const OpException& src) {
	this->errMsg = src.errMsg; //assign the message
	return *this; //and return the instance
}

//move operator
OpException& OpException::operator=(OpException&& src) {
	this->errMsg = src.errMsg; //move the message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* OpException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
