/*
 * BadASTException.cpp
 * Implements an exception that is thrown when a bad AST node is found
 * Created on 2/10/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "BadASTException.h"

//destructor
BadASTException::~BadASTException() {
	//no code needed
}

//overridden what method - called when the exception is thrown
const char* BadASTException::what() const throw() {
	return "error: bad AST node"; //return the error message
}

//no code needed
