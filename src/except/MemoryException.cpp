/*
 * MemoryException.h
 * Implements an exception class that is thrown on a memory error
 * Created on 1/29/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "MemoryException.h"

//constructor
MemoryException::MemoryException()
	: errMsg() //init the field
{	
	//assemble the message
	this->errMsg = "Error: out of memory";
}

//destructor
MemoryException::~MemoryException() {
	//no code needed
}

//copy constructor
MemoryException::MemoryException(const MemoryException& me)
	: errMsg(me.errMsg) //copy the field
{
	//no code needed
}

//move constructor
MemoryException::MemoryException(MemoryException&& me)
	: errMsg(me.errMsg) //move the field
{
	//no code needed
}

//assignment operator
MemoryException& MemoryException::operator=(const MemoryException& src) {
	this->errMsg = src.errMsg; //assign the field
	return *this; //and return the instance
}

//move operator
MemoryException& MemoryException::operator=(MemoryException&& src) {
	this->errMsg = src.errMsg; //move the field
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* MemoryException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
