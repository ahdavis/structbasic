/*
 * OpException.h
 * Declares an exception that is thrown when an invalid operation is found
 * Created on 2/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>
#include <sstream>
#include "../var/VarType.h"

//class declaration
class OpException final : public std::exception
{
	//public fields and methods
	public:
		//constructor 1
		//Used for unary operators
		explicit OpException(VarType badType);

		//constructor 2
		//Used for binary operators
		OpException(VarType badType1, VarType badType2);

		//destructor
		~OpException();

		//copy constructor
		OpException(const OpException& oe);

		//move constructor
		OpException(OpException&& oe);

		//assignment operator
		OpException& operator=(const OpException& src);

		//move operator
		OpException& operator=(OpException&& src);

		//called when the exception is thrown
		const char* what() const throw() override;

	//private fields and methods
	private:
		//field
		std::string errMsg; //the error message


};

//end of header
