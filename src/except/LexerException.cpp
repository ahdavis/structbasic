/*
 * LexerException.cpp
 * Implements an exception that is thrown when the lexer finds an error
 * Created on 2/5/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "LexerException.h"

//constructor
LexerException::LexerException(char badChar)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "(" << curLineNo << ":" << curCharNo << ") "
	   << "error: unknown character " << badChar;
	this->errMsg = ss.str();
}

//destructor
LexerException::~LexerException() {
	//no code needed
}

//assignment operator
LexerException::LexerException(const LexerException& le)
	: errMsg(le.errMsg) //copy the error message
{
	//no code needed
}

//move operator
LexerException::LexerException(LexerException&& le)
	: errMsg(le.errMsg) //move the error message
{
	//no code needed
}

//assignment operator
LexerException& LexerException::operator=(const LexerException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//move operator
LexerException& LexerException::operator=(LexerException&& src) {
	this->errMsg = src.errMsg; //move the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* LexerException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
