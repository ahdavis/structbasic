/*
 * TypeException.cpp
 * Implements an exception that is thrown when an invalid type is found
 * Created on 1/30/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "TypeException.h"

//constructor
TypeException::TypeException(VarType expected, VarType received)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss; //used to assemble the error message
	ss << "error: expected " << typeName(expected) << ", ";
	ss << "received " << typeName(received);

	//and assign the error message
	this->errMsg = ss.str();
}

//destructor
TypeException::~TypeException() {
	//no code needed
}

//copy constructor
TypeException::TypeException(const TypeException& te)
	: errMsg(te.errMsg) //copy the message
{
	//no code needed
}

//move constructor
TypeException::TypeException(TypeException&& te)
	: errMsg(te.errMsg) //move the message
{
	//no code needed
}

//assignment operator
TypeException& TypeException::operator=(const TypeException& src) {
	this->errMsg = src.errMsg; //assign the message
	return *this; //and return the instance
}

//move operator
TypeException& TypeException::operator=(TypeException&& src) {
	this->errMsg = src.errMsg; //move the message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* TypeException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
