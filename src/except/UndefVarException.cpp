/*
 * UndefVarException.cpp
 * Implements an exception that is thrown when an 
 * undefined variable is found
 * Created on 2/14/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "UndefVarException.h"

//constructor
UndefVarException::UndefVarException(const std::string& badName)
	: errMsg("error: undefined variable " + badName) //init the field
{
	//no code needed
}

//destructor
UndefVarException::~UndefVarException() {
	//no code needed
}

//copy constructor
UndefVarException::UndefVarException(const UndefVarException& uve)
	: errMsg(uve.errMsg) //copy the field
{
	//no code needed
}

//move constructor
UndefVarException::UndefVarException(UndefVarException&& uve)
	: errMsg(uve.errMsg) //move the field
{
	//no code needed
}

//assignment operator
UndefVarException& UndefVarException::operator=(
		const UndefVarException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//move operator
UndefVarException& UndefVarException::operator=(UndefVarException&& src) {
	this->errMsg = src.errMsg; //move the error message
	return *this; //and return the instance
}

//overidden what method - called when the exception is thrown
const char* UndefVarException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
