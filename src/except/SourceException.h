/*
 * SourceException.h
 * Declares an exception that is thrown when reading source code fails
 * Created on 2/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//include
#include <exception>
#include <string>

//class declaration
class SourceException final : public std::exception
{
	//public fields and methods
	public:
		//constructor
		explicit SourceException(const std::string& fileName);
		
		//destructor
		~SourceException();
		
		//copy constructor
		SourceException(const SourceException& se);

		//move constructor
		SourceException(SourceException&& se);
		
		//assignment operator
		SourceException& operator=(const SourceException& src);

		//move operator
		SourceException& operator=(SourceException&& src);

		//called when the exception is thrown
		const char* what() const throw() override;

	//private fields and methods
	private:
		//field
		std::string errMsg; //the error message
};

//end of header
