/*
 * TypeException.h
 * Declares an exception that is thrown when an invalid type is found
 * Created on 1/30/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <string>
#include <sstream>
#include <exception>
#include "../var/VarType.h"

//class declaration
class TypeException final : public std::exception
{
	//public fields and methods
	public:
		//constructor
		TypeException(VarType expected, VarType received);

		//destructor
		~TypeException();

		//copy constructor
		TypeException(const TypeException& te);

		//move constructor
		TypeException(TypeException&& te);

		//assignment operator
		TypeException& operator=(const TypeException& src);

		//move operator
		TypeException& operator=(TypeException&& src);

		//other method
		
		//called when the exception is thrown
		const char* what() const throw() override;

	//private fields and methods
	private:
		//field
		std::string errMsg; //the error message
};

//end of header
