/*
 * Parser.h
 * Declares a class that parses StructBASIC code
 * Created on 2/10/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <memory>
#include "Lexer.h"
#include "Token.h"
#include "TokenType.h"
#include "../ast/ASTNode.h"
#include "../ast/OpNode.h"
#include "../ast/ValNode.h"
#include "../ast/AsgnNode.h"
#include "../ast/VarNode.h"
#include "../ast/CmpNode.h"
#include "../ast/OpType.h"
#include "../ast/CmpType.h"
#include "../ast/CallNode.h"
#include "../ast/ColNode.h"
#include "../ast/ForNode.h"
#include "../ast/WhileNode.h"
#include "../ast/IfNode.h"
#include "../except/ParserException.h"
#include "../util/isElement.h"
#include "../util/defs.h"
#include "../func/FuncID.h"
#include "../var/Variant.h"

//class declaration
class Parser final {
	//public fields and methods
	public:
		//constructor
		explicit Parser(const Lexer& newLexer);

		//destructor
		~Parser();

		//copy constructor
		Parser(const Parser& p);

		//move constructor
		Parser(Parser&& p);

		//assignment operator
		Parser& operator=(const Parser& src);
	
		//move operator
		Parser& operator=(Parser&& src);

		//returns an AST derived from the lexer's input
		std::shared_ptr<ASTNode> parse();

	//private fields and methods
	private:
		//methods
		
		//utility methods
		void error(); //throws a ParserException
		void eat(TokenType tokenType); //processes a token

		//parsing methods
		std::shared_ptr<ASTNode> factor(); //parses a factor
		std::shared_ptr<ASTNode> comp(); //parses a comparison
		std::shared_ptr<ASTNode> term(); //parses a term
		std::shared_ptr<ASTNode> expr(); //parses an expression
		std::shared_ptr<ASTNode> stmt(); //parses a statement

		//fields
		Lexer lexer; //the lexer used by the parser
		Token curToken; //the current token being processed

};

//end of header
