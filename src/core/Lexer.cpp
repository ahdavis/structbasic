/*
 * Lexer.cpp
 * Implements a class that lexes StructBASIC code
 * Created on 2/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "Lexer.h"

//constructor
Lexer::Lexer(const std::string& newText)
	: pos(0), curChar('\0'), text(newText) //init the fields
{
	//init the current character
	this->curChar = this->text[this->pos];

	//reset the current character and line number
	curLineNo = 1;
	curCharNo = 1;
}

//destructor
Lexer::~Lexer() {
	//no code needed
}

//copy constructor
Lexer::Lexer(const Lexer& l)
	: pos(l.pos), curChar(l.curChar), text(l.text) //copy the fields
{
	//no code needed
}

//move constructor
Lexer::Lexer(Lexer&& l)
	: pos(l.pos), curChar(l.curChar), text(l.text) //move the fields
{
	//no code needed
}

//assignment operator
Lexer& Lexer::operator=(const Lexer& src) {
	this->pos = src.pos; //assign the position
	this->curChar = src.curChar; //assign the current character
	this->text = src.text; //assign the text
	return *this; //and return the instance
}

//move operator
Lexer& Lexer::operator=(Lexer&& src) {
	this->pos = src.pos; //move the position
	this->curChar = src.curChar; //move the current character
	this->text = src.text; //move the text
	return *this; //and return the instance
}

//getNextToken method - returns the next token retrieved from the input
Token Lexer::getNextToken() {
	//loop through the text
	while(this->curChar != '\0') {
		//handle spaces
		if(isSpace(this->curChar)) {
			this->skipWhitespace(); //skip the whitespace
			continue; //and restart the loop
		}

		//handle numbers
		if(isDigit(this->curChar)) {
			return Token(TokenType::NUM, this->number());
		}

		//handle strings
		if(this->curChar == '"') {
			return Token(TokenType::STR, this->string());
		}

		//handle characters
		if(this->curChar == '\'') {
			return Token(TokenType::CHR, this->character());
		}

		//Booleans are now handled as symbols
		//due to the lexer handling uppercase
		//Ts and Fs in variable names as booleans

		//handle symbols
		if(isAlpha(this->curChar)) {
			return this->symbol(); //lex a symbol
		}

		//handle colons
		if(this->curChar == ':') {
			this->advance(); //advance the lexer

			//and return a colon token
			return Token(TokenType::COL, ':');
		}

		//handle operator tokens
		
		//handle addition and incrementation tokens
		if(this->curChar == '+') {
			this->advance(); //advance the lexer
			//handle incrementation
			if(this->curChar == '+') { //incrementation op
				this->advance(); //advance the lexer
				//and return a token
				return Token(TokenType::INC, "++");
			} else { //addition op
				return Token(TokenType::ADD, '+');
			}
		}
		
		//handle subtraction and decrementation tokens
		if(this->curChar == '-') {
			this->advance(); //advance the lexer
			//handle decrementation
			if(this->curChar == '-') { //decrementation op
				this->advance(); //advance the lexer
				//and return a token
				return Token(TokenType::DEC, "--");
			} else { //subtraction op
				return Token(TokenType::SUB, '-');
			}
		}

		//handle multiplication tokens
		if(this->curChar == '*') {
			this->advance(); //advance the lexer
			return Token(TokenType::MPY, '*');
		}

		//handle division tokens
		if(this->curChar == '/') {
			this->advance(); //advance the lexer
			return Token(TokenType::DIV, '/');
		}

		//handle modulus tokens
		if(this->curChar == '%') {
			this->advance(); //advance the lexer
			return Token(TokenType::MOD, '%');
		}

		//handle comparison tokens
		
		//handle inequality operators
		if(this->curChar == '!') {
			this->advance(); //advance the lexer
			if(this->curChar != '=') { 
				this->error(); //not the correct form
			}
			this->advance(); //advance the lexer
			return Token(TokenType::NEQ, "!=");
		}

		//handle less-than operators
		if(this->curChar == '<') {
			this->advance(); //advance the lexer
			if(this->curChar == '=') { //LTE token
				this->advance();
				return Token(TokenType::LTE, "<=");
			} else { //LT token
				return Token(TokenType::LT, '<');
			}
		}

		//handle greater-than operators
		if(this->curChar == '>') {
			this->advance(); //advance the lexer
			if(this->curChar == '=') { //GTE token
				this->advance();
				return Token(TokenType::GTE, ">=");
			} else { //GT token
				return Token(TokenType::GT, '>');
			}
		}

		//other tokens
		
		//handle a left parenthesis
		if(this->curChar == '(') {
			this->advance(); //advance the lexer
			return Token(TokenType::LPN, '(');
		}

		//handle a right parenthesis
		if(this->curChar == ')') {
			this->advance(); //advance the lexer
			return Token(TokenType::RPN, ')');
		}

		//handle an assignment or equality operator
		if(this->curChar == '=') {
			this->advance(); //advance the lexer
			if(this->curChar == '=') { //equality operator
				this->advance(); //advance the lexer
				//return an equality token
				return Token(TokenType::EQU, "==");
			} else { //assignment operator
				//return an assignment token
				return Token(TokenType::AGN, '=');
			}
		}

		//handle a comma
		if(this->curChar == ',') {
			this->advance(); //advance the lexer
			//and return a comma token
			return Token(TokenType::CMA, ','); 
		}

		//no match, so throw an exception
		this->error();
	}

	//return an EOL token type
	return Token(TokenType::EOL);
}

//private number method - returns a number from the input
Variant Lexer::number() {
	//declare variables
	std::stringstream ss; //used to assemble the number
	bool foundPoint = false; //has a decimal point been found?
	
	//loop and assemble the number
	while((this->curChar != '\0') && ((isDigit(this->curChar)) ||
				(this->curChar == '.'))) {
		//check for a decimal
		if(this->curChar == '.') {
			if(foundPoint) { //if a decimal was already found
				this->error(); //then throw an error
			}
			foundPoint = true; //set the point flag
			ss << this->curChar; //and add the decimal
		} else { //if the character is a digit
			ss << this->curChar; //add the digit
		}

		//advance the lexer
		this->advance();
	}

	//get the string from the stream
	std::string num = ss.str();

	//and determine whether a float or a int was found
	if(foundPoint) { //float
		return Variant(std::stod(num)); //return the float
	} else { //int
		return Variant(std::stoi(num)); //return the integer
	}
}

//private string method - returns a string from the input
Variant Lexer::string() {
	//declare variables
	std::stringstream ss; //used to assemble the string

	//advance the lexer past the initial quotation
	this->advance();

	//assemble the string
	while(this->curChar != '"') {
		ss << this->curChar; //append the character
		this->advance(); //and advance the lexer
	}

	//advance past the second quotation
	this->advance();

	//and return the string
	return Variant(ss.str());
}

//private character method - returns a character from the input
Variant Lexer::character() {
	//advance past the first single quote
	this->advance();

	//get the actual character and advance
	char ret = this->curChar; 
	this->advance();

	//advance past the second single quote
	this->advance();

	//and return the character
	return Variant(ret);
}

//private symbol method - lexes a symbol (variable, keyword, etc)
Token Lexer::symbol() {
	//declare a buffer
	std::stringstream ss;

	//loop and populate the buffer
	while(isAlpha(this->curChar) || isDigit(this->curChar)) {
		ss << this->curChar; //append the current character
		this->advance(); //and advance the lexer
	}

	//get the string from the buffer
	std::string sym = ss.str();

	//handle keywords
	if(sym == "true") { //true keyword
		return Token(TokenType::BOL, true); //return a true token
	} else if(sym == "false") { //false keyword
		return Token(TokenType::BOL, false); //return a false token
	} else if(sym == "input") { //input function call
		return Token(TokenType::BCL, "input"); //return an input
	} else if(sym == "print") { //print function call
		return Token(TokenType::BCL, "print"); //return a print
	} else if(sym == "println") { //println function call
		return Token(TokenType::BCL, "println"); //return a println
	} else if(sym == "eval") { //eval function call
		return Token(TokenType::BCL, "eval"); //return an eval
	} else if(sym == "for") { //for loop
		return Token(TokenType::FOR, "for"); //return a for token
	} else if(sym == "do") { //do statement
		return Token(TokenType::DO, "do"); //return a do token
	} else if(sym == "end") { //end statement
		return Token(TokenType::END, "end"); //return an end token
	} else if(sym == "while") { //while loop
		return Token(TokenType::WHL, "while"); //return a while
	} else if(sym == "if") { //if statement
		return Token(TokenType::IF, "if"); //return an if
	} else if(sym == "then") { //then directive
		return Token(TokenType::THN, "then"); //return a then
	} else if(sym == "else") { //else directive
		return Token(TokenType::ELS, "else"); //return an else
	} else { //variable name
		//return a variable token from the buffer
		return Token(TokenType::VAR, sym);
	}
}

//private advance method - advances the lexer
void Lexer::advance() {
	this->pos++; //advance the position

	//update the character and line numbers
	if(this->curChar == '\n') { //if the end of the line is found
		curLineNo++; //then update the line number
		curCharNo = 1; //and reset the character number
	} else { //no end of line
		curCharNo++; //update the character number
	}

	//update the character
	if(this->pos > (this->text.length() - 1)) { //end of input
		this->curChar = '\0'; //null the current char
	} else { //not end of input
		this->curChar = this->text[this->pos];
	}
}

//private skipWhitespace method - skips whitespace in the input
void Lexer::skipWhitespace() {
	//loop through the whitespace
	while((this->curChar != '\0') && (isSpace(this->curChar))) {
		this->advance(); //advance the lexer
	}
}

//private error method - throws a LexerException
void Lexer::error() {
	throw LexerException(this->curChar); //throw an exception
}

//end of implementation
