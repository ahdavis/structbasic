/*
 * Parser.cpp
 * Implements a class that parses StructBASIC code
 * Created on 2/10/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "Parser.h"

//constructor
Parser::Parser(const Lexer& newLexer)
	: lexer(newLexer), curToken(TokenType::EOL) //init the fields
{
	//init the current token
	this->curToken = this->lexer.getNextToken();
}

//destructor
Parser::~Parser() {
	//no code needed
}

//copy constructor
Parser::Parser(const Parser& p)
	: lexer(p.lexer), curToken(p.curToken) //copy the fields
{
	//no code needed
}

//move constructor
Parser::Parser(Parser&& p)
	: lexer(p.lexer), curToken(p.curToken) //move the fields
{
	//no code needed
}

//assignment operator
Parser& Parser::operator=(const Parser& src) {
	this->lexer = src.lexer; //assign the lexer
	this->curToken = src.curToken; //assign the current token
	return *this; //and return the instance
}

//move operator
Parser& Parser::operator=(Parser&& src) {
	this->lexer = src.lexer; //move the lexer
	this->curToken = src.curToken; //move the current token
	return *this; //and return the instance
}

//parse method - returns an AST derived from the lexer's input
std::shared_ptr<ASTNode> Parser::parse() {
	return this->stmt(); //return a parsed statement
}

//private error method - throws a ParserException
void Parser::error() {
	throw ParserException(); //throw a parser exception
}

//private eat method - processes a token and gets the next token
void Parser::eat(TokenType tokenType) {
	//make sure the tokens match
	if(this->curToken.getType() == tokenType) { //if they match
		//then get the next token
		this->curToken = this->lexer.getNextToken();
	} else { //if they don't match
		this->error(); //then throw an exception
	}
}

//private factor method - parses a factor
std::shared_ptr<ASTNode> Parser::factor() {
	//localize the current token
	Token token = this->curToken;

	//handle different token types
	if(token.getType() == TokenType::NUM) { //number token
		this->eat(TokenType::NUM); //get the next token
		//and return a node
		return std::make_shared<ValNode>(token.getValue()); 
	} else if(token.getType() == TokenType::BOL) { //boolean token
		this->eat(TokenType::BOL); //get the next token
		//and return a node
		return std::make_shared<ValNode>(token.getValue());
	} else if(token.getType() == TokenType::CHR) { //character token
		this->eat(TokenType::CHR); //get the next token
		//and return a node
		return std::make_shared<ValNode>(token.getValue()); 
	} else if(token.getType() == TokenType::STR) { //string token
		this->eat(TokenType::STR); //get the next token
		//and return a node
		return std::make_shared<ValNode>(token.getValue());
	} else if(token.getType() == TokenType::VAR) { //variable token
		this->eat(TokenType::VAR); //get the next token
		//and return a node
		return std::make_shared<VarNode>(token
						.getValue()
							.stringValue());
	} else if(token.getType() == TokenType::LPN) { //left parenthesis
		this->eat(TokenType::LPN); //get the next token
		//parse the expression
		std::shared_ptr<ASTNode> node = this->stmt(); 
		this->eat(TokenType::RPN); //get the next token
		return node; //and return the node
	} else if(token.getType() == TokenType::SUB) { //negation
		this->eat(TokenType::SUB); //get the next token
		//and return a node
		return std::make_shared<OpNode>(OpType::NEG, 
				this->factor(), 
					nullptr);
	} else if(token.getType() == TokenType::BCL) { //function call
		//get the name of the function to execute
		Variant funcName = token.getValue();
		
		this->eat(TokenType::BCL); //get the next token
		this->eat(TokenType::LPN); //skip the left parenthesis
		
		//loop and get the function arguments
		ArgList args; //the arguments to the function
		while(this->curToken.getType() != TokenType::RPN) {
			//add a parsed expression to the args
			args.push_back(this->stmt());

			//handle commas
			if(this->curToken.getType() == TokenType::CMA) {
				this->eat(TokenType::CMA);
			}
		}

		//skip the right parenthesis
		this->eat(TokenType::RPN);

		//get the function to execute
		FuncID funcID;
		if(funcName == "input") { //input function
			funcID = FuncID::INPUT; //init the function ID
		} else if(funcName == "print") { //print function
			funcID = FuncID::PRINT; //init the function ID
		} else if(funcName == "eval") { //eval function
			funcID = FuncID::EVAL; //init the function ID
		} else { //println function
			funcID = FuncID::PRINTLN; //init the function ID
		}

		//and return a function node
		return std::make_shared<CallNode>(funcID, args);

	} else if(this->curToken.getType() == TokenType::FOR) { //for loop
		this->eat(TokenType::FOR); //get the next token
		//get the loop elements
		std::shared_ptr<ASTNode> init = this->stmt();
		this->eat(TokenType::CMA); //eat the comma
		std::shared_ptr<ASTNode> cond = this->stmt(); 
		this->eat(TokenType::CMA); //eat the comma
		std::shared_ptr<ASTNode> inc = this->stmt();
		this->eat(TokenType::DO); //eat the loop start
		std::shared_ptr<ASTNode> body = this->stmt();
		this->eat(TokenType::END); //eat the loop end

		//generate the loop node
		return std::make_shared<ForNode>(init, cond, inc, body);
	} else if(this->curToken.getType() == TokenType::WHL) { //while loop
		this->eat(TokenType::WHL); //get the next token
		//get the loop elements
		std::shared_ptr<ASTNode> cond = this->stmt();
		this->eat(TokenType::DO); //eat the loop start
		std::shared_ptr<ASTNode> body = this->stmt();
		this->eat(TokenType::END); //eat the loop end

		//generate the loop node
		return std::make_shared<WhileNode>(cond, body);
	} else if(this->curToken.getType() == TokenType::IF) { //if stmt
		this->eat(TokenType::IF); //get the next token
		
		//get the statement elements
		std::shared_ptr<ASTNode> cond = this->stmt();
		this->eat(TokenType::THN); //eat the then branch
		std::shared_ptr<ASTNode> then = this->stmt();
		
		//handle whether the statement has an else branch
		std::shared_ptr<ASTNode> els = nullptr;
		if(this->curToken.getType() == TokenType::ELS) { 
			this->eat(TokenType::ELS); //eat the else token
			els = this->stmt(); //get the else branch
		}

		//finish parsing the statement
		this->eat(TokenType::END); //eat the statement end

		//and assemble the node
		return std::make_shared<IfNode>(cond, then, els);
	}

	//if control reaches here, there is a syntax error
	//so throw an exception
	this->error();

	return nullptr; //appease the compiler
}

//private comp method - parses a comparison
std::shared_ptr<ASTNode> Parser::comp() {
	//parse a factor
	std::shared_ptr<ASTNode> node = this->factor();

	//process the comparisons
	while(isElement(this->curToken.getType(), {TokenType::EQU,
							TokenType::GT,
							TokenType::GTE,
							TokenType::LT,
							TokenType::LTE,
							TokenType::NEQ})) {
		//localize the current token
		Token token = this->curToken;

		//declare the comparison type
		CmpType cmpType;

		//handle the different comparisons
		if(token.getType() == TokenType::EQU) { //equality
			this->eat(TokenType::EQU); //get the next token
			cmpType = CmpType::EQU; //and set the comp type
		} else if(token.getType() == TokenType::GT) { //greater
			this->eat(TokenType::GT); //get the next token
			cmpType = CmpType::GT; //and set the comp type
		} else if(token.getType() == TokenType::GTE) { 
			this->eat(TokenType::GTE); //get the next token
			cmpType = CmpType::GTE; //and set the comp type
		} else if(token.getType() == TokenType::LT) { //less
			this->eat(TokenType::LT); //get the next token
			cmpType = CmpType::LT; //and set the comp type
		} else if(token.getType() == TokenType::LTE) { 
			this->eat(TokenType::LTE); //get the next token
			cmpType = CmpType::LTE; //and set the comp type
		} else { //inequality
			this->eat(TokenType::NEQ); //get the next token
			cmpType = CmpType::NEQ; //and set the comp type
		}

		//update the node
		node = std::make_shared<CmpNode>(cmpType, node, 
							this->factor());
	}

	//and return the node
	return node;

}

//private term method - parses a term
std::shared_ptr<ASTNode> Parser::term() {
	//parse a comparison
	std::shared_ptr<ASTNode> node = this->comp();

	//process the assignment operator
	if(this->curToken.getType() == TokenType::AGN) {
		this->eat(TokenType::AGN); //get the next token
		//and update the node
		node = std::make_shared<AsgnNode>(node, this->factor());
	}

	//process *, /, and % operations
	
	while(isElement(this->curToken.getType(), {TokenType::DIV, 
							TokenType::MOD,
							TokenType::MPY})) {

		//localize the current token
		Token token = this->curToken;

		//declare an operation type
		OpType opType;

		//handle the operations
		if(token.getType() == TokenType::MPY) { //multiply token
			this->eat(TokenType::MPY); //get the next token
			opType = OpType::MPY; //and set the op type
		} else if(token.getType() == TokenType::DIV) { //div token
			this->eat(TokenType::DIV); //get the next token
			opType = OpType::DIV; //and set the op type
		} else { //mod token
			this->eat(TokenType::MOD); //get the next token
			opType = OpType::MOD; //and set the op type
		}

		//and update the AST
		node = std::make_shared<OpNode>(opType, node, 
				this->factor());
	}

	//and return the node
	return node;
}

//private expr method - parses an expression
std::shared_ptr<ASTNode> Parser::expr() {
	//parse a term
	std::shared_ptr<ASTNode> node = this->term();

	//handle + and - operations
	while(isElement(this->curToken.getType(), {TokenType::ADD,
							TokenType::DEC,
							TokenType::INC,
							TokenType::SUB})) {
		//localize the current token
		Token token = this->curToken;

		//declare an operation type
		OpType opType;

		//parse the operations
		if(token.getType() == TokenType::ADD) { //addition token
			this->eat(TokenType::ADD); //get the next token
			opType = OpType::ADD; //and set the op type
		} else if(token.getType() == TokenType::DEC) { //decrement
			this->eat(TokenType::DEC); //get the next token
			opType = OpType::DEC; //and set the op type
		} else if(token.getType() == TokenType::INC) { //increment
			this->eat(TokenType::INC); //get the next token
			opType = OpType::INC; //and set the op type
		} else { //subtraction token
			this->eat(TokenType::SUB); //get the next token
			opType = OpType::SUB; //and set the op type
		}

		//get the right node
		std::shared_ptr<ASTNode> right;

		//and determine whether to use it
		if((opType == OpType::DEC) || (opType == OpType::INC)) {
			right = nullptr; //null the node
		} else { //if the node is not a decrement or increment node
			right = this->term();
		}

		//and update the AST
		node = std::make_shared<OpNode>(opType, node, right);
	}

	//and return the node
	return node;
}

//private stmt method - parses a statement
std::shared_ptr<ASTNode> Parser::stmt() {
	std::shared_ptr<ASTNode> node = this->expr(); //parse an expression

	//handle statements
	while(isElement(this->curToken.getType(),
				{ TokenType::COL })) 
	{
		//localize the current token
		Token token = this->curToken;

		//parse the statements
		if(token.getType() == TokenType::COL) { //colon token
			this->eat(TokenType::COL); //get the next token

			//and generate the node
			node = std::make_shared<ColNode>(node, 
							this->expr());
		}
	}

	//return the node
	return node;
}


//end of implementation
