/*
 * TokenType.h
 * Enumerates types of tokens for StructBASIC
 * Created on 2/4/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//no includes

//enum definition
enum class TokenType {
	EOL, //end of line token
	NUM, //number token
	CHR, //character token
	STR, //string token
	BOL, //boolean token
	ADD, //addition token
	SUB, //subtraction token
	MPY, //multiplication token
	DIV, //division token
	MOD, //modulus token
	LPN, //left parenthesis token
	RPN, //right parenthesis token
	AGN, //assignment operator token
	VAR, //variable token
	INC, //increment token
	DEC, //decrement token
	LT, //less-than token
	GT, //greater-than token
	LTE, //less-than-or-equal-to token
	GTE, //greater-than-or-equal-to token
	EQU, //equal token
	NEQ, //not-equal token
	BCL, //built-in function call token
	CMA, //comma
	COL, //colon
	FOR, //for loop token
	WHL, //while loop token
	DO, //do statement token
	IF, //if statement token
	THN, //then statement token
	ELS, //else statement token
	END, //end of block token
};

//end of implementation
