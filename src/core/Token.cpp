/*
 * Token.cpp
 * Implements a class that represents a StructBASIC token
 * Created on 2/4/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "Token.h"

//first constructor
Token::Token(TokenType newType, const Variant& newValue)
	: type(newType), value(newValue) //init the fields
{
	//no code needed
}

//second constructor
Token::Token(TokenType newType)
	: Token(newType, Variant()) //call the other constructor
{
	//no code needed
}

//destructor
Token::~Token() {
	//no code needed
}

//copy constructor
Token::Token(const Token& t)
	: type(t.type), value(t.value) //copy the fields
{
	//no code needed
}

//move constructor
Token::Token(Token&& t)
	: type(t.type), value(t.value) //move the fields
{
	//no code needed
}

//assignment operator
Token& Token::operator=(const Token& src) {
	this->type = src.type; //assign the type field
	this->value = src.value; //assign the value field
	return *this; //and return the instance
}

//move operator
Token& Token::operator=(Token&& src) {
	this->type = src.type; //move the type field
	this->value = src.value; //move the value field
	return *this; //and return the instance
}

//getType method - returns the type of the Token
TokenType Token::getType() const {
	return this->type; //return the type field
}

//getValue method - returns the value of the Token
const Variant& Token::getValue() const {
	return this->value; //return the value field
}

//isValueType method - returns whether the Token holds a value
bool Token::isValueType() const {
	if(this->type == TokenType::BOL) { //boolean
		return true;
	} else if(this->type == TokenType::CHR) { //character
		return true;
	} else if(this->type == TokenType::NUM) { //number
		return true;
	} else if(this->type == TokenType::STR) { //string
		return true;
	} else { //not a value
		return false;
	}
}

//isOpType method - returns whether the Token holds an operation
bool Token::isOpType() const {
	if(this->type == TokenType::ADD) { //addition
		return true;
	} else if(this->type == TokenType::DIV) { //division
		return true;
	} else if(this->type == TokenType::MOD) { //modulus
		return true;
	} else if(this->type == TokenType::MPY) { //multiplication
		return true;
	} else if(this->type == TokenType::SUB) { //subtraction
		return true;
	} else { //not an op
		return false;
	}
}

//end of implementation
