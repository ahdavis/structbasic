/*
 * Interpreter.h
 * Declares a class that interprets StructBASIC code
 * Created on 2/10/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <iostream>
#include <memory>
#include "../var/Variant.h"
#include "../ast/ASTNode.h"
#include "../ast/NodeType.h"
#include "../ast/OpNode.h"
#include "../ast/OpType.h"
#include "../ast/ValNode.h"
#include "../core/Parser.h"
#include "../except/NullASTException.h"
#include "../except/BadASTException.h"
#include "../except/UnknownOpException.h"
#include "../var/Variable.h"
#include "../var/VarRegistry.h"

//class declaration
class Interpreter final {
	//public fields and methods
	public:
		//constructor
		explicit Interpreter(const Parser& newParser);

		//destructor
		~Interpreter();

		//copy constructor
		Interpreter(const Interpreter& i);

		//move constructor
		Interpreter(Interpreter&& i);

		//assignment operator
		Interpreter& operator=(const Interpreter& src);

		//move operator
		Interpreter& operator=(Interpreter&& src);

		//interprets an AST generated by the parser
		Variant interpret();

	//private fields and methods
	private:
		//methods
		
		//evaluates an AST
		Variant eval(const std::shared_ptr<ASTNode>& ast);

		//field
		Parser parser; //the parser that generates the AST

};

//end of header
