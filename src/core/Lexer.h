/*
 * Lexer.h
 * Declares a class that lexes StructBASIC code
 * Created on 2/8/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <string>
#include <sstream>
#include "TokenType.h"
#include "Token.h"
#include "../util/data.h"
#include "../except/LexerException.h"
#include "../util/typecheck.h"
#include "../util/isDigit.h"
#include "../util/isSpace.h"
#include "../util/isAlpha.h"
#include "../var/Variant.h"

//class declaration
class Lexer final {
	//public fields and methods
	public:
		//constructor
		explicit Lexer(const std::string& newText);

		//destructor
		~Lexer();

		//copy constructor
		Lexer(const Lexer& l);

		//move constructor
		Lexer(Lexer&& l);

		//assignment operator
		Lexer& operator=(const Lexer& src);

		//move operator
		Lexer& operator=(Lexer&& src);

		//other method
		
		//returns the next token consumed from the input
		Token getNextToken();

	//private fields and methods
	private:
		//methods
		
		//value-retrieving methods
		Variant number(); //returns a number from the input
		Variant string(); //returns a string from the input
		Variant character(); //returns a character from the input
		Token symbol(); //returns a symbol from the input

		//advancement methods
		void advance(); //advances the lexer
		void skipWhitespace(); //skips whitespace

		//other methods
		void error(); //throws a LexerException

		//fields
		unsigned int pos; //the current read position in the text
		char curChar; //the current character from the text
		std::string text; //the text to be lexed

};

//end of header
