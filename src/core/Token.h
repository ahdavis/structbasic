/*
 * Token.h
 * Declares a class that represents a StructBASIC token
 * Created on 2/4/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "TokenType.h"
#include "../var/Variant.h"

//class declaration
class Token final {
	//public fields and methods
	public:
		//first constructor
		Token(TokenType newType, const Variant& newValue);

		//second constructor
		explicit Token(TokenType newType);

		//destructor
		~Token();

		//copy constructor
		Token(const Token& t);

		//move constructor
		Token(Token&& t);

		//assignment operator
		Token& operator=(const Token& src);

		//move operator
		Token& operator=(Token&& src);

		//getter methods
		
		//returns the type of the Token
		TokenType getType() const;

		//returns the value of the Token
		const Variant& getValue() const;

		//returns whether the Token is a value Token
		bool isValueType() const;

		//returns whether the Token is an operation Token
		bool isOpType() const;

	//private fields and methods
	private:
		//fields
		TokenType type; //the type of the Token
		Variant value; //the value of the Token

};

//end of header
